//
//  SelecteAreaViewController.h
//  DongYuan
//
//  Created by 鄢罗军 on 14/9/28.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SelecteStationViewController;
@protocol SelecteStationDelegate <NSObject>

-(void)SelecteStationConfirmBtnClicked:(NSString *)selectedStation :(SelecteStationViewController*)selectAreaCtrl;
-(void)SelecteStationCancelBtnClicked:(SelecteStationViewController*)selectAreaCtrl;

@end

@interface SelecteStationViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    IBOutlet UITableView *mainTable;
    NSMutableArray *dataArr;
    
    NSInteger selectedIndex;
}

@property(nonatomic,retain)IBOutlet UIButton *curBtn;
@property(nonatomic,assign)id<SelecteStationDelegate>delegate;
-(void)setStationsArrs:(NSMutableArray *)arr;

-(IBAction)curBtnAction:(id)sender;

-(IBAction)confirmBtnAction:(id)sender;

@end
