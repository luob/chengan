//
//  SelecteAreaViewController.m
//  DongYuan
//
//  Created by 鄢罗军 on 14/9/28.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import "SelecteZNStationViewController.h"

@interface SelecteZNStationViewController ()

@end

@implementation SelecteZNStationViewController
@synthesize curBtn;
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)setStationsArrs:(NSMutableArray *)arr{
    if (!dataArr) {
        dataArr = [NSMutableArray array];
    }
    [dataArr removeAllObjects];
    dataArr = [arr mutableCopy];
    [mainTable reloadData];
    
    selectedIndex = 0 ;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    CGPoint touchLocation=[[touches anyObject] locationInView:self.view];

    CGRect rect = CGRectMake(174, 61, 220, 198);
    if (CGRectContainsPoint(rect,touchLocation)) {
        return;
    }
    [self.view removeFromSuperview];
    if (delegate && [delegate respondsToSelector:@selector(SelecteZNStationCancelBtnClicked:)]) {
        [delegate SelecteZNStationCancelBtnClicked:self];
    }
}
-(IBAction)curBtnAction:(id)sender{
    mainTable.hidden = NO;
}
-(IBAction)confirmBtnAction:(id)sender{
    [self.view removeFromSuperview];
    if (delegate && [delegate respondsToSelector:@selector(SelecteZNStationConfirmBtnClicked::)]) {
        [delegate SelecteZNStationConfirmBtnClicked:dataArr[selectedIndex] :self ];
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArr.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cellid = @"cellid";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellid];
    }
    cell.textLabel.text = dataArr[indexPath.row];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    mainTable.hidden = YES;
    selectedIndex = indexPath.row;
    [curBtn setTitle:dataArr[indexPath.row] forState:UIControlStateNormal];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    mainTable.hidden = YES;

    [curBtn setTitle:dataArr[0] forState:UIControlStateNormal];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
