//
//  SelecteStationAndCheckPointViewController.m
//  DongYuan
//
//  Created by 鄢罗军 on 14/10/9.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import "SelecteStationAndCheckPointViewController.h"

@interface SelecteStationAndCheckPointViewController ()

@end

@implementation SelecteStationAndCheckPointViewController
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(IBAction)StationAction{
    [self.view removeFromSuperview];

    if (delegate && [delegate respondsToSelector:@selector(SelecteStationBtnClicked:)]) {
        [delegate SelecteStationBtnClicked:self];
    }
}
-(IBAction)CheckPointAction{
    [self.view removeFromSuperview];

    if (delegate && [delegate respondsToSelector:@selector(SelecteCheckPointBtnClicked:)]) {
        [delegate SelecteCheckPointBtnClicked:self];
    }

}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    CGPoint touchLocation=[[touches anyObject] locationInView:self.view];
    CGRect rect = CGRectMake(40, 172, 238, 145);
    if (CGRectContainsPoint(rect,touchLocation)) {
        return;
    }

    [self.view removeFromSuperview];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
