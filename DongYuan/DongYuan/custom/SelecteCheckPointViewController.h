//
//  SelecteAreaViewController.h
//  DongYuan
//
//  Created by 鄢罗军 on 14/9/28.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SelecteCheckPointViewController;
@protocol SelecteCheckPointDelegate <NSObject>

-(void)SelecteCheckPointConfirmBtnClicked:(NSString *)selectedStation :(SelecteCheckPointViewController*)selectAreaCtrl;
-(void)SelecteCheckPointCancelBtnClicked:(SelecteCheckPointViewController*)selectAreaCtrl;

@end

@interface SelecteCheckPointViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    IBOutlet UITableView *mainTable;
    NSMutableArray *dataArr;
    
    NSInteger selectedIndex;
}

@property(nonatomic,retain)IBOutlet UIButton *curBtn;
@property(nonatomic,assign)id<SelecteCheckPointDelegate>delegate;
-(void)setStationsArrs:(NSMutableArray *)arr;

-(IBAction)curBtnAction:(id)sender;

-(IBAction)confirmBtnAction:(id)sender;

@end
