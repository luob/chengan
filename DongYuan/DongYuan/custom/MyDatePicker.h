//
//  LauchActivityDatePicker.h
//  LifeVale
//
//  Created by Yuan on 12-8-23.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DatePickerDelegate <NSObject>

-(void)MyDatePickerDidSelectedDate:(NSString *)dateString;

@end

@interface MyDatePicker : UIView{
    UIDatePicker *datapicker; // 选择出生年月
}
@property (nonatomic, assign) id<DatePickerDelegate> delegate;
@end
