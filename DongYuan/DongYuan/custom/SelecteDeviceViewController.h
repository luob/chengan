//
//  SelecteAreaViewController.h
//  DongYuan
//
//  Created by 鄢罗军 on 14/9/28.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SelecteDeviceViewController;
@protocol SelecteDeviceDelegate <NSObject>

-(void)SelecteDeviceConfirmBtnClicked:(NSArray *)selectedArr :(SelecteDeviceViewController*)selectAreaCtrl;
-(void)SelecteDeviceCancelBtnClicked:(SelecteDeviceViewController*)selectAreaCtrl;

@end

@interface SelecteDeviceViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    IBOutlet UITableView *mainTable;
    NSMutableArray *dataArr;
    
    NSInteger selectedIndex;
}

@property(nonatomic,retain)IBOutlet UIButton *curBtn;
@property(nonatomic,assign)id<SelecteDeviceDelegate>delegate;
-(void)setDeviceArrs:(NSMutableArray *)arr;

-(IBAction)curBtnAction:(id)sender;

-(IBAction)confirmBtnAction:(id)sender;

@end
