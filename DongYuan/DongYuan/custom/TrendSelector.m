//
//  SelecteStationAndCheckPointViewController.m
//  DongYuan
//
//  Created by 鄢罗军 on 14/10/9.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import "TrendSelector.h"

@interface TrendSelector ()

@end

@implementation TrendSelector
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(IBAction)TempAction{
    [self.view removeFromSuperview];
    
    if (delegate && [delegate respondsToSelector:@selector(TrendSelectorTempBtnClicked:)]) {
        [delegate TrendSelectorTempBtnClicked:self];
    }
}
-(IBAction)PressAction{
    [self.view removeFromSuperview];
    
    if (delegate && [delegate respondsToSelector:@selector(TrendSelectorPressBtnClicked:)]) {
        [delegate TrendSelectorPressBtnClicked:self];
    }
}
-(IBAction)WaterAction{
    [self.view removeFromSuperview];
    
    if (delegate && [delegate respondsToSelector:@selector(TrendSelectorWaterBtnClicked:)]) {
        [delegate TrendSelectorWaterBtnClicked:self];
    }
}
-(IBAction)GongHaoAction{
    [self.view removeFromSuperview];
    
    if (delegate && [delegate respondsToSelector:@selector(TrendSelectorGongHaoBtnClicked:)]) {
        [delegate TrendSelectorGongHaoBtnClicked:self];
    }
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    CGPoint touchLocation=[[touches anyObject] locationInView:self.view];
    CGRect rect = CGRectMake(40, 172, 238, 145);
    if (CGRectContainsPoint(rect,touchLocation)) {
        return;
    }

    [self.view removeFromSuperview];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
