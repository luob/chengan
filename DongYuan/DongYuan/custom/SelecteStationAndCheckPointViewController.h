//
//  SelecteStationAndCheckPointViewController.h
//  DongYuan
//
//  Created by 鄢罗军 on 14/10/9.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SelecteStationAndCheckPointViewController;
@protocol SelecteStationAndCheckPointDelegate <NSObject>

-(void)SelecteCheckPointBtnClicked:(SelecteStationAndCheckPointViewController*)selectAreaCtrl;
-(void)SelecteStationBtnClicked:(SelecteStationAndCheckPointViewController*)selectAreaCtrl;

@end
@interface SelecteStationAndCheckPointViewController : UIViewController


@property(nonatomic,assign)id<SelecteStationAndCheckPointDelegate>delegate;


-(IBAction)StationAction;
-(IBAction)CheckPointAction;

@end
