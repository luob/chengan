//
//  SelecteAreaViewController.h
//  DongYuan
//
//  Created by 鄢罗军 on 14/9/28.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SelecteAreaViewController;
@protocol SelecteAreaDelegate <NSObject>

-(void)SelecteAreaConfirmBtnClicked:(NSArray *)selectedArr :(SelecteAreaViewController*)selectAreaCtrl;
-(void)SelecteAreaCancelBtnClicked:(SelecteAreaViewController*)selectAreaCtrl;

@end

@interface SelecteAreaViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    IBOutlet UITableView *mainTable;
    NSMutableArray *dataArr;
    
    NSInteger selectedIndex;
}

@property(nonatomic,retain)IBOutlet UIButton *curBtn;
@property(nonatomic,assign)id<SelecteAreaDelegate>delegate;
-(void)setAreaArrs:(NSMutableArray *)arr;

-(IBAction)curBtnAction:(id)sender;

-(IBAction)confirmBtnAction:(id)sender;

@end
