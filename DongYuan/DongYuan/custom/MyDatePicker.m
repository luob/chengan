//
//  MDataPicker.m
//  lifeVale
//
//  Created by Yuan on 12-8-14.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "MyDatePicker.h"
#import <QuartzCore/QuartzCore.h>
#import "constants.h"


@implementation MyDatePicker
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        self.backgroundColor = [UIColor clearColor];
        
        UIView *accosaryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenFrame.size.width, 244)];
        accosaryView.backgroundColor = [UIColor blackColor];
        accosaryView.alpha = 0.5;
        if (iPhone5) {
            accosaryView.frame = CGRectMake(0, 0, ScreenFrame.size.width, 224+88);
        }
        [self addSubview:accosaryView];
        
        UIView *mview = [[UIView alloc] initWithFrame: CGRectMake(0.0f, 244, ScreenFrame.size.width, 216.f)];
        if (iPhone5) {
            mview.frame = CGRectMake(0.0f, 224+88, ScreenFrame.size.width, 216.f);
        }
        [self addSubview:mview];
        
        datapicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 40, ScreenFrame.size.width, 216)];
         datapicker.datePickerMode = UIDatePickerModeDateAndTime;
        datapicker.backgroundColor = [UIColor whiteColor];
//        datapicker.maximumDate = [NSDate dateWithTimeIntervalSinceNow:kMaxLaunchActivity_Date];
        datapicker.maximumDate = [NSDate date];
        [mview addSubview:datapicker];
        
        UIToolbar *topToolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, ScreenFrame.size.width, 40)];
        topToolBar.barStyle = UIBarStyleBlack;
        
        UIBarButtonItem *canelitem = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStyleBordered target:self action:@selector(canelitemAction)];
        //        canelitem.width = 45.0f;
        
        UIBarButtonItem *spaceItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        
        UIBarButtonItem *safeitem = [[UIBarButtonItem alloc] initWithTitle:@"选择" style:UIBarButtonItemStyleBordered target:self action:@selector(safeitemAction)]; 
        
        NSArray *items = [NSArray arrayWithObjects:canelitem, spaceItem, safeitem, nil];
        [topToolBar setItems:items];
        
        [mview addSubview:topToolBar];
     }
    return self;
}
-(void)canelitemAction{
    [delegate MyDatePickerDidSelectedDate:nil];
}
-(void)safeitemAction{
    
    NSString *dateStr = nil;
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy-MM-dd hh:mm"];
    dateStr = [NSString stringWithFormat:@"%@:0", [format stringFromDate:datapicker.date]];
 
    
    [delegate MyDatePickerDidSelectedDate:dateStr];
}


@end
