//
//  SelecteZNStationViewController.h
//  DongYuan
//
//  Created by 鄢罗军 on 14/10/13.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SelecteZNStationViewController;
@protocol SelecteZNStationDelegate <NSObject>

-(void)SelecteZNStationConfirmBtnClicked:(NSString *)selectedStation :(SelecteZNStationViewController*)selectAreaCtrl;
-(void)SelecteZNStationCancelBtnClicked:(SelecteZNStationViewController*)selectAreaCtrl;

@end

@interface SelecteZNStationViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    IBOutlet UITableView *mainTable;
    NSMutableArray *dataArr;
    
    NSInteger selectedIndex;
}

@property(nonatomic,retain)IBOutlet UIButton *curBtn;
@property(nonatomic,assign)id<SelecteZNStationDelegate>delegate;
-(void)setStationsArrs:(NSMutableArray *)arr;

-(IBAction)curBtnAction:(id)sender;

-(IBAction)confirmBtnAction:(id)sender;
@end
