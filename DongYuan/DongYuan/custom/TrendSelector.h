//
//  SelecteStationAndCheckPointViewController.h
//  DongYuan
//
//  Created by 鄢罗军 on 14/10/9.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TrendSelector;
@protocol TrendSelectorDelegate <NSObject>

-(void)TrendSelectorTempBtnClicked:(TrendSelector*)selectCtrl;
-(void)TrendSelectorPressBtnClicked:(TrendSelector*)selectCtrl;
-(void)TrendSelectorWaterBtnClicked:(TrendSelector*)selectCtrl;
-(void)TrendSelectorGongHaoBtnClicked:(TrendSelector*)selectCtrl;

@end
@interface TrendSelector : UIViewController


@property(nonatomic,assign)id<TrendSelectorDelegate>delegate;


-(IBAction)TempAction;
-(IBAction)PressAction;
-(IBAction)WaterAction;
-(IBAction)GongHaoAction;

@end
