//
//  aViewController.h
//  BonyIntelligentForIphone
//
//  Created by 鄢罗军 on 14-8-4.
//  Copyright (c) 2014年 鄢 罗军. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface leftCustNavBtn: UIView
{
    id target;
    SEL action;
    
    
}
@property (nonatomic)id target;
@property (nonatomic)SEL action;

@property (nonatomic, retain)NSString *title;
@property (nonatomic, retain)UIImageView*imageview;
@property (nonatomic, retain)UILabel*titlelb;

+(leftCustNavBtn *)leftCustNavBtnWithTitle:(NSString *)title image:(UIImage *)image target:(id)tg action:(SEL)action;

-(void)SetStatusToConnected:(BOOL)inNet;
-(void)SetStatusToConnected;
-(void)SetStatusToUnConnected;
@end


@interface UIViewController (AddCustomNavigationbar)
-(UIView *)AddCustomNavigationBar;
-(UILabel *)AddCustomNavigationbarTitle:(NSString *)title;
-(leftCustNavBtn *)AddCustomLeftImageItem:(UIImage *)image title:(NSString *)title WithSelector:(SEL)selector Target:(id)target;
-(void)AddCustomLeftImageItem:(UIImage *)image WithSelector:(SEL)selector Target:(id)target;
-(void)AddCustomRightImageItem:(UIImage *)image WithSelector:(SEL)selector Target:(id)target;
-(UIButton *)AddCustomRightWithTitle:(NSString *)title WithSelector:(SEL)selector Target:(id)target;
-(void)AddSwipGesture;
@end
