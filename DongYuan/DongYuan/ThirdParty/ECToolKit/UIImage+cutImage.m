//
//  UIImageView.m
//  MicroAgent
//
//  Created by 鄢罗军 on 14-7-31.
//  Copyright (c) 2014年 鄢 罗军. All rights reserved.
//

#import "UIImage+cutImage.h"

@implementation UIImage (cutImage)
////截取截取大小为需要显示的大小。取图片中间位置截取
+(UIImage*)imageWithImage:(UIImage*)image cutToSize:(CGSize)newSize
{
     //裁剪
    float x;
    float y;
    float width;
    float height;
    
    float scale; //根据传入的imageview的size来决定

     /*
    //按照imageview的固定尺寸 裁剪 （一般不建议）
      
    x=(image.size.width-newSize.width)/2;
    y=(image.size.height-newSize.height)/2;
    CGRect myImageRect = CGRectMake(x, y, newSize.width, newSize.height);
     */
    
    //按照imageview的大小比例裁剪 （建议）
    //图片是长方形
    if (image.size.width < image.size.height) {
        scale = newSize.height/newSize.width;
        x = 0;
        width = image.size.width;
        height = width*scale;
        if (height>image.size.height) { //目标高度超出图片的实际高度，则裁剪宽度 高度保持不变
            height = image.size.height;
            y=0;
        }else{
            y = (image.size.height-height)/2;  //裁剪高度
        }
    }else if (image.size.height < image.size.width) {
        scale = newSize.width/newSize.height;
        y = 0;
        height = image.size.height;
        width = height*scale;
        if (width>image.size.width) { //目标宽度超出图片的实际宽度，则裁剪高度 宽度保持不变
            x = 0;
            width = image.size.width;
        }else{
            x = (image.size.width-image.size.height)/2; //裁剪宽度
        }
    }else{ //图片是正方形
        if (newSize.width > newSize.height) {           //裁剪宽度
            y=0;
            scale = newSize.width/newSize.height;
            height = image.size.height;
            width = height*scale;
            x = (image.size.width-width)/2;
        }else if(newSize.height > newSize.width){         //裁剪高度
            x=0;
            scale = newSize.height/newSize.width;
            width = image.size.width;
            height = width*scale;
            y = (image.size.height-height)/2;
        }else{                                            //保持原图
            x=0;
            y=0;
            width = image.size.width;
            height = image.size.height;
        }
    }

    CGRect myImageRect = CGRectMake(x, y, width, height);
    
    UIImage* bigImage= image;
    image = nil;
    CGImageRef imageRef = bigImage.CGImage;
    CGImageRef subImageRef = CGImageCreateWithImageInRect(imageRef, myImageRect);
    
    UIGraphicsBeginImageContext(CGSizeMake(width,height));
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextDrawImage(context, myImageRect, subImageRef);
    UIImage* newImage = [UIImage imageWithCGImage:subImageRef];
    UIGraphicsEndImageContext();
    return newImage;
}
//根据图片尺寸对图片进行压缩
+(UIImage*)imageWithImage:(UIImage*)image scaleToSize:(CGSize)newSize{
    //缩放
     UIGraphicsBeginImageContext(newSize);
     [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
     UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
     UIGraphicsEndImageContext();
    return newImage;
}
@end



