



#import "UIViewController+custNav.h"
#import "ECToolKit.h"
#import "constants.h"
 
@implementation leftCustNavBtn
@synthesize title,target,action,imageview,titlelb;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)SetStatusToUnConnected{
    imageview.image = [UIImage imageNamed:@"未连接"];
    titlelb.text = @"未连接..";
    titlelb.textColor = [UIColor ColorWithHexString:@"fb2503"];
}
+(leftCustNavBtn *)leftCustNavBtnWithTitle:(NSString *)title image:(UIImage *)image target:(id)tg action:(SEL)action{
    
    CGSize size = [title sizeWithFont:[UIFont systemFontOfSize:18] constrainedToSize:CGSizeMake(MAXFLOAT, 44)];
    if (size.width < 50) {
        size.width = 50;
    }
    
    leftCustNavBtn*btn = [[self alloc] initWithFrame:CGRectMake(0, GetTopHeight, image.size.width+size.width, 44)];
    btn.title = title;
    btn.target = tg;
    btn.action = action;

    btn.imageview = [[UIImageView alloc] initWithFrame:CGRectMake(5, (44-image.size.height)/2, image.size.width, image.size.height)];
    btn.imageview.image = image;
    [btn addSubview:btn.imageview];
    
    btn.titlelb = [[UILabel alloc] initWithFrame:CGRectMake(image.size.width+5, 5, size.width, 34)];
    btn.titlelb.backgroundColor = [UIColor clearColor];
    btn.titlelb.font = [UIFont systemFontOfSize:15];
    btn.titlelb.adjustsFontSizeToFitWidth = YES;
    btn.titlelb.textColor = [UIColor whiteColor];
    btn.titlelb.textAlignment = NSTextAlignmentCenter;
    [btn addSubview:btn.titlelb];
    btn.titlelb.text = title;
    
    UIButton *bb = [UIButton buttonWithType:UIButtonTypeCustom];
    bb.frame = CGRectMake(0,0,image.size.width+size.width,44);
    [bb addTarget:tg action:action forControlEvents:UIControlEventTouchUpInside];
     [btn addSubview:bb];
    return btn;
}

@end
@implementation  UIViewController (AddCustomNavigationbar)
-(UIView *)AddCustomNavigationBar{
    
    UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 44+GetTopHeight)];
    image.image = [[UIImage imageNamed:@"home_01.png"] stretchableImageWithLeftCapWidth:2 topCapHeight:10];
    [self.view addSubview:image];
//    image.backgroundColor = [UIColor ColorWithHexString:@"21292c"];
    return image;
}
-(UILabel *)AddCustomNavigationbarTitle:(NSString *)title{
    
    UILabel *titlelb = [[UILabel alloc] initWithFrame:CGRectMake(50, GetTopHeight, 220, 44)];
    titlelb.textColor = [UIColor whiteColor];
    titlelb.text = title;
    titlelb.textAlignment = NSTextAlignmentCenter;
    titlelb.backgroundColor = [UIColor clearColor];
    titlelb.font = [UIFont systemFontOfSize:kTitleFont];
    [self.view addSubview:titlelb];
    titlelb.userInteractionEnabled = NO;
    return titlelb;
}
-(leftCustNavBtn *)AddCustomLeftImageItem:(UIImage *)image title:(NSString *)title WithSelector:(SEL)selector Target:(id)target{
    leftCustNavBtn *btn = [leftCustNavBtn leftCustNavBtnWithTitle:title image:image target:target action:selector];
    [self.view addSubview:btn];
    return btn;
}
-(void)AddCustomLeftImageItem:(UIImage *)image WithSelector:(SEL)selector Target:(id)target{
     UIButton *BackBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    BackBtn.frame = CGRectMake(0, GetTopHeight, 50, 44);
    [BackBtn addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    [BackBtn setImage:image forState:UIControlStateNormal];
    [self.view addSubview:BackBtn];
}
-(void)AddCustomRightImageItem:(UIImage *)image WithSelector:(SEL)selector Target:(id)target{
    UIButton *BackBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    BackBtn.frame = CGRectMake(270, GetTopHeight, 50, 44);
    [BackBtn addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    [BackBtn setImage:image forState:UIControlStateNormal];
    [self.view addSubview:BackBtn];
}
-(UIButton *)AddCustomRightWithTitle:(NSString *)title WithSelector:(SEL)selector Target:(id)target{
    UIButton *BackBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    CGSize size = [title sizeWithFont:[UIFont systemFontOfSize:18] constrainedToSize:CGSizeMake(MAXFLOAT, 44)];
    if (size.width < 50) {
        size.width = 50;
    }
    BackBtn.frame = CGRectMake(ScreenFrame.size.width-size.width-10, GetTopHeight, size.width, 44);
    [BackBtn addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    [BackBtn setTitle:title forState:UIControlStateNormal];
    BackBtn.titleLabel.font = [UIFont systemFontOfSize:18];
    [self.view addSubview:BackBtn];
    return BackBtn;
}

-(void)AddSwipGesture{
    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(backAction)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:recognizer];
}
@end


