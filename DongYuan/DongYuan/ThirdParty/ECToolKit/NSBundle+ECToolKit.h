//
//  NSBundle+SSToolkitAdditions.h
//  SSToolkit
//
//  Created by Sam Soffes on 3/22/12.
//  Copyright (c) 2012 Sam Soffes. All rights reserved.
//

#define SSToolkitLocalizedString(key) [[NSBundle ssToolkitBundle] localizedStringForKey:(key) value:@"" table:@"SSToolkit"]

@interface NSBundle (ECToolKit)

+ (NSBundle *)ssToolkitBundle;
//获取document目录文件路径
+(NSString *)documentFilePath;
//获取temp目录文件路径
+(NSString *)tempFilePath;
@end
