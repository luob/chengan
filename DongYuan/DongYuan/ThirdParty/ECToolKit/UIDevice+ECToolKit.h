//
//  UIDevice+SSToolkitAdditions.h
//  SSToolkit
//
//  Created by Sam Soffes on 7/13/09.
//  Copyright 2009-2011 Sam Soffes. All rights reserved.
//

/**
 Provides extensions to `UIDevice` for various common tasks.
 */
@interface UIDevice (ECToolKit)

/**
 Returns `YES` if the device is a simulator.
 
 @return `YES` if the device is a simulator and `NO` if it is not.
 */
- (BOOL)isSimulator;

/**
 Returns `YES` if the device is an iPod touch, iPhone, iPhone 3G, or an iPhone 3GS.
 
 @return `YES` if the device is crappy and `NO` if it is not.
 */
- (BOOL)isCrappy;

+(BOOL)isIphone5;

+(BOOL)isIOS7;

#pragma mark 取系统版本
+(NSString *)getSystemVersion;

#pragma mark 取系统名
+(NSString *)getSystemName;

//返回 iPhone 5C (GSM+CDMA)
+(NSString *)platformString;

/*
 * app bundle identifier   com.ailk.eagent
 
 能保证在一个app里面 获取到的是 唯一值 (卸载程序值也不会被修改)
 */
+ (NSString*)UDID;

@end



