//
//  UIColor+ECToolKit.h
//  ECToolKit
//
//  Created by 鄢罗军 on 14-7-9.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIColor (UIColor_ECToolKit)
+(UIColor *)ColorWithHexString:(NSString *)hex;
@end
