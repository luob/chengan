//
//  NSString+encry.h
//  MicroAgent
//
//  Created by 鄢罗军 on 14-8-15.
//  Copyright (c) 2014年 鄢 罗军. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (encrypto)
- (NSString *) md5;
- (NSString *) sha1;
- (NSString *) sha1_base64;
- (NSString *) md5_base64;
- (NSString *) base64;
@end
