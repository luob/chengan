//
//  ECSqliteUtil.h
//  BonyIntelligentForIphone
//
//  Created by 鄢罗军 on 14-7-22.
//  Copyright (c) 2014年 鄢 罗军. All rights reserved.
//

 #import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "FMDatabaseQueue.h"
@interface ECSqliteUtil : NSObject
@property (nonatomic, strong) FMDatabaseQueue *databaseQueue;

#pragma mark 执行删除和更新命令
-(void)doSqlForUpdate:(NSString *)sql;
#pragma mark 执行创建表命令
-(void)doSqlForCreatWithTableName:(NSString *)tableName model:(id)model;
#pragma mark 执行插入命令
-(void)doSqlForInsertWithTableName:(NSString *)tableName model:(id)model;
#pragma mark 执行更新命令
-(void)doSqlForUpdateWithTableName:(NSString *)tableName model:(id)model;
#pragma mark 执行查询命令
-(NSMutableArray *)doSqlForSelect:(NSString *)sql modelClass:(Class)className;
@end
