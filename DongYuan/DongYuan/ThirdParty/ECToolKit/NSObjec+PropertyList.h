//
//  NSObjec+PropertyList.h
//  BonyIntelligentForIphone
//
//  Created by 鄢罗军 on 14-7-21.
//  Copyright (c) 2014年 鄢 罗军. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>
@interface NSObject (PropertyListing)
/* 获取对象的所有属性 */
- (NSMutableDictionary *)properties_aps;
@end