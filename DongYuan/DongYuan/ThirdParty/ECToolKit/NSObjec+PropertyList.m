//
//  NSObjec+PropertyList.m
//  BonyIntelligentForIphone
//
//  Created by 鄢罗军 on 14-7-21.
//  Copyright (c) 2014年 鄢 罗军. All rights reserved.
//

#import "NSObjec+PropertyList.h"

@implementation NSObject (PropertyListing)
/* 获取对象的所有属性 */
- (NSMutableDictionary *)properties_aps
{
    NSMutableDictionary *props = [NSMutableDictionary dictionary];
    unsigned int outCount, i;
    objc_property_t *properties = class_copyPropertyList([self class], &outCount);
    for (i = 0; i<outCount; i++)
    {
        objc_property_t property = properties[i];
        const char* char_f =property_getName(property);
        NSString *propertyName = [NSString stringWithUTF8String:char_f];
        id propertyValue = [self valueForKey:(NSString *)propertyName];
        if (propertyValue) {
            [props setObject:propertyValue forKey:propertyName];
        }else{
            [props setObject:[NSNull null] forKey:propertyName];
        }
    }
    free(properties);
    return props;
}
@end
