//
//  ECSqliteUtil.m
//  BonyIntelligentForIphone
//
//  Created by 鄢罗军 on 14-7-22.
//  Copyright (c) 2014年 鄢 罗军. All rights reserved.
//

#import "ECSqliteUtil.h"
#import "NSObjec+PropertyList.h"

@implementation ECSqliteUtil
@synthesize databaseQueue;
//初始化
-(id) init{
    self = [super init];
    if(self){
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *path = [paths objectAtIndex:0];
        path = [path stringByAppendingString:@"/eagent.db"];
        
        //删除现有的文件
        NSFileManager *fm = [NSFileManager defaultManager];
        //    [fm removeItemAtPath: path error: nil];
        
        if (![fm fileExistsAtPath:path isDirectory:NO]) {
            [fm copyItemAtPath:[[NSBundle mainBundle ] pathForResource:@"eagent.db" ofType:@""] toPath:path error:nil];
        }
        
        self.databaseQueue = [FMDatabaseQueue databaseQueueWithPath:path];
         
    }
    return self;
}

#pragma mark 执行删除和更新命令
-(void)doSqlForUpdate:(NSString *)sql{
    [self.databaseQueue inDatabase:^(FMDatabase *database){
        [database executeUpdate:sql];
    }];
}
#pragma mark 执行创建表命令
-(void)doSqlForCreatWithTableName:(NSString *)tableName model:(id)model{
    NSMutableString *sqlStr = [NSMutableString string];
    [sqlStr appendFormat:@"CREATE TABLE IF NOT EXISTS %@ (",tableName];
    
    NSMutableDictionary *propertys = [model properties_aps];
    for (NSString *propertyName in [propertys allKeys]) {
        [sqlStr appendFormat:@"%@",propertyName];
        [sqlStr appendFormat:@" "];
        id value = [propertys objectForKey:propertyName];
        if ([value isKindOfClass:[NSString class]]) {
            [sqlStr appendFormat:@"text"];
        }else{
            [sqlStr appendFormat:@"numeric"];
        }
        [sqlStr appendFormat:@","];
    }
    [sqlStr deleteCharactersInRange:NSMakeRange(sqlStr.length-1,1)];
    [sqlStr appendFormat:@")"];
    [self.databaseQueue inDatabase:^(FMDatabase *database){
        [database executeUpdate:sqlStr];
    }];
}
#pragma mark 执行插入命令
-(void)doSqlForInsertWithTableName:(NSString *)tableName model:(id)model{
    NSMutableString *sqlStr = [NSMutableString string];
    [sqlStr appendFormat:@"insert into %@ (",tableName];
    NSMutableDictionary *propertys = [model properties_aps];
    for (NSString *propertyName in [propertys allKeys]) {
        [sqlStr appendFormat:@"%@",propertyName];
        if (![[[propertys allKeys] lastObject] isEqualToString:propertyName]) {
            [sqlStr appendFormat:@","];
        }
    }
    [sqlStr appendFormat:@")"];
    [sqlStr appendFormat:@"values ("];
    
    for (id propertyValue in [propertys allValues]) {
        if ([propertyValue isEqual:[NSNull null]]) {
            [sqlStr appendFormat:@"''"];
        }else{
            [sqlStr appendFormat:@"'%@'",propertyValue];
        }
        [sqlStr appendFormat:@","];
    }
    [sqlStr deleteCharactersInRange:NSMakeRange(sqlStr.length-1,1)];
    [sqlStr appendFormat:@")"];
    [self.databaseQueue inDatabase:^(FMDatabase *database){
        [database executeUpdate:sqlStr];
    }];
}
#pragma mark 执行更新命令
-(void)doSqlForUpdateWithTableName:(NSString *)tableName model:(id)model{
    
    NSMutableString *sqlStr = [NSMutableString string];
    [sqlStr appendFormat:@"UPDATE %@ set ",tableName];
    NSMutableDictionary *propertys = [model properties_aps];
    for (NSString *propertyName in [propertys allKeys]) {
        [sqlStr appendFormat:@"%@",propertyName];
        [sqlStr appendFormat:@"="];
        id value = [propertys objectForKey:propertyName];
        if ([value isEqual:[NSNull null]]) {
            [sqlStr appendFormat:@"''"];
        }else{
            [sqlStr appendFormat:@"'%@'",value];
        }
        [sqlStr appendFormat:@","];
    }
    
    [sqlStr deleteCharactersInRange:NSMakeRange(sqlStr.length-1,1)];
//    if ([model isKindOfClass:[RoomModel class]]) {
//        [sqlStr appendFormat:@" where room_id = '%@'",[(RoomModel *)model room_id]];
//    }
//    if ([model isKindOfClass:[DeviceModel class]]) {
//        [sqlStr appendFormat:@" where dev_id = '%@'",[(DeviceModel *)model dev_id]];
//    }
//    if ([model isKindOfClass:[FuncModel class]]) {
//        [sqlStr appendFormat:@" where func_id = '%@'",[(FuncModel *)model func_id]];
//    }
    [self.databaseQueue inDatabase:^(FMDatabase *database){
        [database executeUpdate:sqlStr];
    }];
}
#pragma mark 执行查询命令
-(NSMutableArray *)doSqlForSelect:(NSString *)sql modelClass:(Class)className{
    NSMutableArray *ar = [[NSMutableArray alloc] initWithCapacity:1];
    [self.databaseQueue inDatabase:^(FMDatabase *database){
        FMResultSet *rs = [database executeQuery:sql];
        while ([rs next]) {
            [ar addObject:[rs resultDictionary]];
        };
        [rs close];
    }];
    NSMutableArray *result = [[NSMutableArray alloc] initWithCapacity:1];
    for (NSDictionary *dic in ar) {
        NSObject *info = [[className alloc] init];
        
        NSMutableDictionary *propertys = [info properties_aps];
        for (NSString *propertyName in [propertys allKeys]) {
            id value = [dic objectForKey:propertyName];
            if (value) {
                [info setValue:value forKey:propertyName];
            }else{
                [info setValue:[NSNull null] forKey:propertyName];
            }
        }
        [result addObject:info];
        info = nil;
    }
    return result;
}
@end
