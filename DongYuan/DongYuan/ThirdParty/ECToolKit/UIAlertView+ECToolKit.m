//
//  UIAlertView+ShowAlert.m
//  SSToolkit
//
//  Created by 鄢罗军 on 14-3-7.
//  Copyright (c) 2014年 Sam Soffes. All rights reserved.
//

#import "UIAlertView+ECToolKit.h"

@implementation UIAlertView (ECToolKit)

+(void)ShowAlertViewWithMsg:(NSString *)msg{
    [self ShowAlertViewWithTitle:@"提示" MSG:msg];
}
+(void)ShowAlertViewWithTitle:(NSString *)title MSG:(NSString *)msg{
        UIAlertView *v = [[UIAlertView alloc] initWithTitle:title message:msg delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles: nil];
        [v show];
}
@end
