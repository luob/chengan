//
//  UIImageView.h
//  MicroAgent
//
//  Created by 鄢罗军 on 14-7-31.
//  Copyright (c) 2014年 鄢 罗军. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIImage (cutImage)
//根据图片尺寸对图片进行裁剪
+(UIImage*)imageWithImage:(UIImage*)image cutToSize:(CGSize)newSize;
//根据图片尺寸对图片进行压缩
+(UIImage*)imageWithImage:(UIImage*)image scaleToSize:(CGSize)newSize;
@end
