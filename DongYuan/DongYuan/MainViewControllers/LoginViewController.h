//
//  LoginViewController.h
//  DongYuan
//
//  Created by 鄢罗军 on 14/9/19.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController{
    IBOutlet UITextField *userField,*passField,*ipField,*portField;
}

-(IBAction)LoginAction:(id)sender;
@end
