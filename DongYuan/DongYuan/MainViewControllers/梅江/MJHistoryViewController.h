//
//  HistoryNormalStationViewController.h
//  DongYuan
//
//  Created by 鄢罗军 on 14/10/13.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import "BASEUIViewController.h"
#import "SelecteAreaViewController.h"
#import "SelecteStationViewController.h"
#import "SelecteDeviceViewController.h"
#import "MyDatePicker.h"

#import "SelecteCheckPointViewController.h"
@interface MJHistoryViewController : BASEUIViewController<UITableViewDataSource,UITableViewDelegate,SelecteAreaDelegate,SelecteStationDelegate,SelecteDeviceDelegate,DatePickerDelegate>
{
    
    UITableView*mainTable;
    NSMutableArray*dataArr;
    
    UISwitch*daySwitch,*weekSwitch,*monthSwitch;
    UISwitch*hourSwitch;
    
    NSMutableArray*stationNames;
    
    SelecteCheckPointViewController*selectCheckPointCtrl;
     
    NSString *startTime,*endTime;
    
    MyDatePicker *datepicker;
    
    NSInteger curDateIndex;
    
    BOOL hourswitch;
}
-(IBAction)ConfirmAction;
@end






