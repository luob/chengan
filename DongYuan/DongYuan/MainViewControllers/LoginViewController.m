//
//  LoginViewController.m
//  DongYuan
//
//  Created by 鄢罗军 on 14/9/19.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import "LoginViewController.h"
#import "HomeViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSString*ip = [[NSUserDefaults standardUserDefaults] objectForKey:@"ip"];
    NSString*port = [[NSUserDefaults standardUserDefaults] objectForKey:@"port"];
    if (ip && ![ip isEqualToString:@""]) {
        ipField.text = ip;
    }
    if (port && ![port isEqualToString:@""]) {
        portField.text = port;
    }
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
-(IBAction)LoginAction:(id)sender{
    
    if (!ipField.text || [ipField.text isEqualToString:@""] || !portField.text || [portField.text isEqualToString:@""]) {
        [UIAlertView ShowAlertViewWithMsg:@"请输入ip和端口"];
        return;
    }
    NSString *url = [NSString stringWithFormat:@"http://%@:%@/service.asmx",ipField.text,portField.text];
    SharedDelegate.WebServiceUrl = url;
    
    
    [[NSUserDefaults standardUserDefaults] setObject:ipField.text forKey:@"ip"];
    [[NSUserDefaults standardUserDefaults] setObject:portField.text forKey:@"port"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    HomeViewController*home = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    UINavigationController*NavCtrl = [[UINavigationController alloc] initWithRootViewController:home];
    NavCtrl.navigationBarHidden = YES;
    [self presentViewController:NavCtrl animated:NO completion:nil];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end




