//
//  TempTrendView.m
//  DongYuan
//
//  Created by 鄢罗军 on 14/10/14.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import "CheckPointTrendView.h"

@implementation CheckPointTrendView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
-(void)AddHeadView{
    UILabel*titlelb = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 100, 30)];
    titlelb.text = @"单位:℃";
    titlelb.backgroundColor = [UIColor clearColor];
    titlelb.font = [UIFont boldSystemFontOfSize:16];
    [self addSubview:titlelb];
    
    {
        UILabel*lb = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width-160, 5, 70, 20)];
        lb.text = @"--供温";
        lb.backgroundColor = [UIColor clearColor];
        lb.textColor = [UIColor redColor];
        lb.font = [UIFont systemFontOfSize:14];
        [self addSubview:lb];
    }
    {
        UILabel*lb = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width-80, 5, 70, 20)];
        lb.text = @"--回温";
        lb.backgroundColor = [UIColor clearColor];
        lb.textColor = [UIColor greenColor];
        lb.font = [UIFont systemFontOfSize:14];
        [self addSubview:lb];
    }
    
}
-(void)setOneGoTempArr:(NSMutableArray*)arr OneBackTempArr:(NSMutableArray*)arr2{
    if (!OneGoTArr) {
        OneGoTArr = [NSMutableArray array];
    }
    OneGoTArr = [arr mutableCopy];
    if (!OneBackTArr) {
        OneBackTArr = [NSMutableArray array];
    }
    OneBackTArr = [arr2 mutableCopy];
    
    PNLineChartView_landscape *lineChartView = [[PNLineChartView_landscape alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    lineChartView.backgroundColor = [UIColor whiteColor];
    [self addSubview:lineChartView];
    
    lineChartView.max = 120;
    lineChartView.min = 0;
    
    lineChartView.interval = (lineChartView.max-lineChartView.min)/10;
    NSMutableArray*xAxisValues = [NSMutableArray array];
    for (id value in arr) {
        [xAxisValues addObject:[NSString stringWithFormat:@"%d",[arr indexOfObject:value]+1]];
    }
    
    NSMutableArray* yAxisValues = [@[] mutableCopy];
    for (int i=0; i<11; i++) {
        NSString* str = [NSString stringWithFormat:@"%.2f", lineChartView.min+lineChartView.interval*i];
        [yAxisValues addObject:str];
    }
    lineChartView.xAxisValues = xAxisValues;
    lineChartView.yAxisValues = yAxisValues;
    lineChartView.axisLeftLineWidth = 30;
    
    {
        PNPlot *plot1 = [[PNPlot alloc] init];
        plot1.plottingValues = OneGoTArr;
        plot1.lineColor = [UIColor redColor];
        plot1.lineWidth = 0.5;
        [lineChartView addPlot:plot1];
    }
    {
        PNPlot *plot1 = [[PNPlot alloc] init];
        plot1.plottingValues = OneBackTArr;
        plot1.lineColor = [UIColor greenColor];
        plot1.lineWidth = 0.5;
        [lineChartView addPlot:plot1];
    }
    
    [self AddHeadView];
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
