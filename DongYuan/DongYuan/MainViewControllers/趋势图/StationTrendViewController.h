//
//  TrendViewController.h
//  DongYuan
//
//  Created by 鄢罗军 on 14/9/19.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BASEUIViewController.h"
#import "PNLineChartView.h"
#import "PNPlot.h"
#import "TempTrendView.h"
#import "PressTrendView.h"
#import "WebServices.h"

#import "TrendSelector.h"

@interface StationTrendViewController :BASEUIViewController<WebServiceDelegate,TrendSelectorDelegate>
{
    WebServices *helper;

    NSString *curstationName,*curdevid,*curstartTime,*curendTime,*curArea;
    BOOL curhour;
    
    NSMutableArray*dataArr;
    NSInteger page,totoalPage;
     
    NSMutableArray*OneGoTArr,*OneBackTArr,*OneGoPArr,*OneBackPArr;
    
    TempTrendView *TTView;
    PressTrendView *PTView;
    TrendSelector*trender;
}
 
-(void)requestWithStationName:(NSString *)stationName deviceid:(NSString *)devid curArea:(NSString*)Area startTime:(NSString *)startTime endTime:(NSString *)endTime housSwitch:(BOOL)hour;

@end
