//
//  CheckPointTrendViewController.m
//  DongYuan
//
//  Created by 鄢罗军 on 14/10/14.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import "CheckPointTrendViewController.h"
#import "SoapHelper.h"
#import "ASIDataDecompressor.h"
#import "Base64.h"
#import "JSON.h"

#import "CheckPointTrendView.h"

@interface CheckPointTrendViewController ()

@end

@implementation CheckPointTrendViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)backAction{
    [self dismissViewControllerAnimated:NO completion:nil];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44+GetTopHeight)];
    image.image = [[UIImage imageNamed:@"home_01.png"] stretchableImageWithLeftCapWidth:2 topCapHeight:10];
    [self.view addSubview:image];
    
    UILabel*titlelb = [[UILabel alloc] initWithFrame:CGRectMake(0, GetTopHeight, self.view.frame.size.width, 44)];
    titlelb.text = @"关口趋势图";
    titlelb.textAlignment = NSTextAlignmentCenter;
    titlelb.textColor = [UIColor whiteColor];
    titlelb.font = [UIFont systemFontOfSize:18];
    [self.view addSubview:titlelb];
    
    UIButton *BackBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    BackBtn.frame = CGRectMake(0, GetTopHeight, 50, 44);
    [BackBtn addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [BackBtn setImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
    [self.view addSubview:BackBtn];
    
    helper=[[WebServices alloc] initWithDelegate:self];
    page = 1;
    [self getJAHistoryDataGWC_Select];
}
- (void)getJAHistoryDataGWC_Select{
    
    if (!dataArr) {
        dataArr = [NSMutableArray array];
    }
    [dataArr removeAllObjects];
    [maintable reloadData];
    
    [AppHelper showHUD:@"正在获取关口数据" inView:self.view];
    NSMutableArray *arr=[NSMutableArray array];
    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"20",@"pageSize", nil]];
    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%d",page],@"pageIndex", nil]];
    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:curStartTime,@"startTime", nil]];
    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:curendTime,@"endTime", nil]];
    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:curCheckPointName,@"gwcName", nil]];
    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:curhour?@"1":@"0",@"type", nil]];
    
    NSString *soapMsg=[SoapHelper arrayToDefaultSoapMessage:arr methodName:@"CAHistoryDataGWC_Select"];
    [helper asyncServiceMethod:@"CAHistoryDataGWC_Select" soapMessage:soapMsg];
}
-(void)requestFinishedMessageForCount:(NSString *)count{
    NSInteger totoal = [count intValue]/20;
    if ([count intValue]%20!=0) {
        totoal++;
    }
    totoalPage = totoal;
    totoalPagelb.text = [NSString stringWithFormat:@"共 %d 页",totoal];
}
#pragma mark -
#pragma mark WebServices delegate Methods
-(void)requestFinishedMessage:(NSString*)xml{
    [AppHelper removeHUD];
    NSData*data = [Base64 decodeBase64WithString:xml];
    NSError *error = nil;
    NSData*tempData = [ASIDataDecompressor uncompressData:data error:&error];
    
    NSStringEncoding gbkEncoding = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
    {
        NSString *xmlstr = [[NSString alloc] initWithData:tempData encoding:gbkEncoding];
        NSLog(@"返回的xml数据:%@",xmlstr);
        NSString*preSrt = [xmlstr componentsSeparatedByString:@"@"][0];
        NSLog(@"preSrt:%@",preSrt);
        NSString*sufSrt = [xmlstr componentsSeparatedByString:@"@"][1];
        
        if (!dataArr) {
            dataArr = [NSMutableArray array];
        }
        [dataArr removeAllObjects];
        dataArr = [[sufSrt JSONValue] mutableCopy];
        
        if (!goTempArr) {
            goTempArr = [NSMutableArray array];
        }
        [goTempArr removeAllObjects];
        if (!backTempArr) {
            backTempArr = [NSMutableArray array];
        }
        [backTempArr removeAllObjects];
        for (NSArray*ar in dataArr) {
            NSString*OneGoTStr = [NSString stringWithFormat:@"%f",[ar[2] floatValue]];
            [goTempArr addObject:OneGoTStr];
            
            NSString*OneBackTStr = [NSString stringWithFormat:@"%f",[ar[3] floatValue]];
            [backTempArr addObject:OneBackTStr];
         }
        {
            CheckPointTrendView *CPTView = [[CheckPointTrendView alloc] initWithFrame:CGRectMake(0, GetToTopDistance, ScreenFrame.size.height, self.view.frame.size.height-GetToTopDistance)];
            CPTView.backgroundColor = [UIColor whiteColor];
            [self.view addSubview:CPTView];
            [CPTView setOneGoTempArr:goTempArr OneBackTempArr:backTempArr];
        }

    }
    
    if (error) {
        NSLog(@"%@",[error description]);
    }
}
-(void)requestFailedMessage:(NSError*)error{
    [AppHelper removeHUD];
    NSLog(@"请求失败:%@\n",[error description]);
}

-(void)requestWithCheckPointName:(NSString *)checkPointName startTime:(NSString *)startTime endTime:(NSString *)endTime housSwitch:(BOOL)hour;{
    
    curStartTime = startTime;
    curendTime = endTime;
    curCheckPointName = checkPointName;
    curhour = hour;
}
-(BOOL)shouldAutorotate{
    return YES;
}
-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscapeRight;//这里写你需要的方向。看好了，别写错了
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
