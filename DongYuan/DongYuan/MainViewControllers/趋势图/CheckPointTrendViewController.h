//
//  CheckPointTrendViewController.h
//  DongYuan
//
//  Created by 鄢罗军 on 14/10/14.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import "BASEUIViewController.h"
#import "WebServices.h"
@interface CheckPointTrendViewController : BASEUIViewController
<WebServiceDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>{
    WebServices *helper;
    IBOutlet UILabel*toplb;
    NSMutableArray*dataArr;
    UITableView*maintable;
    NSString *curCheckPointName,*curendTime,*curStartTime;
    BOOL curhour;
    
    int page;
    
    UILabel*totoalPagelb;
    
    UITextField*curPageField;
    
    UIView*inputPageView;
    UITextField*inputPageField;
    
    int totoalPage;
    
    
    NSMutableArray*goTempArr,*backTempArr;
}

-(void)requestWithCheckPointName:(NSString *)checkPointName startTime:(NSString *)startTime endTime:(NSString *)endTime housSwitch:(BOOL)hour;
@end
