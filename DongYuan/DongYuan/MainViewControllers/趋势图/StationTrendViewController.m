//
//  TrendViewController.m
//  DongYuan
//
//  Created by 鄢罗军 on 14/9/19.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import "StationTrendViewController.h"
#import "ECToolKit.h"
#import "SoapHelper.h"
#import "ASIDataDecompressor.h"
#import "Base64.h"
#import "JSON.h"

#import "PNLineChartView_landscape.h"
#import "PNPlot.h"



@interface StationTrendViewController ()

@end

@implementation StationTrendViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        TTView.hidden = NO;
        PTView.hidden = YES;
        [self.view bringSubviewToFront:TTView];
    }
    if (buttonIndex == 2) {
        TTView.hidden = YES;
        PTView.hidden = NO;
        [self.view bringSubviewToFront:PTView];

    }
    if (buttonIndex == 3) {
        TTView.hidden = YES;
        PTView.hidden = YES;
        [UIAlertView ShowAlertViewWithMsg:@"暂无数据"];
    }
    if (buttonIndex == 4) {
        TTView.hidden = YES;
        PTView.hidden = YES;
        [UIAlertView ShowAlertViewWithMsg:@"暂无数据"];
    }
}

-(void)TrendSelectorTempBtnClicked:(TrendSelector *)selectCtrl{
    TTView.hidden = NO;
    PTView.hidden = YES;
    [self.view bringSubviewToFront:TTView];
}
-(void)TrendSelectorPressBtnClicked:(TrendSelector *)selectCtrl{
    TTView.hidden = YES;
    PTView.hidden = NO;
    [self.view bringSubviewToFront:PTView];

}
-(void)TrendSelectorWaterBtnClicked:(TrendSelector *)selectCtrl{
    TTView.hidden = YES;
    PTView.hidden = YES;
    [UIAlertView ShowAlertViewWithMsg:@"暂无数据"];

}
-(void)TrendSelectorGongHaoBtnClicked:(TrendSelector *)selectCtrl{
    TTView.hidden = YES;
    PTView.hidden = YES;
    [UIAlertView ShowAlertViewWithMsg:@"暂无数据"];

}
-(void)menuBtnAction{
    trender = [[TrendSelector alloc] initWithNibName:@"TrendSelector" bundle:nil];
    trender.delegate = self;
    [self.view addSubview:trender.view];
}
-(void)backAction{
    [self dismissViewControllerAnimated:NO completion:nil];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44+GetTopHeight)];
    image.image = [[UIImage imageNamed:@"home_01.png"] stretchableImageWithLeftCapWidth:2 topCapHeight:10];
    [self.view addSubview:image];
     
    UILabel*titlelb = [[UILabel alloc] initWithFrame:CGRectMake(0, GetTopHeight, self.view.frame.size.width, 44)];
    titlelb.text = @"站点趋势图";
    titlelb.textAlignment = NSTextAlignmentCenter;
    titlelb.textColor = [UIColor whiteColor];
    titlelb.font = [UIFont systemFontOfSize:18];
    [self.view addSubview:titlelb];
    
    UIButton *BackBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    BackBtn.frame = CGRectMake(0, GetTopHeight, 50, 44);
    [BackBtn addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [BackBtn setImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
    [self.view addSubview:BackBtn];
    
    UIButton *menuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    menuBtn.frame = CGRectMake(0, 0, 150, GetToTopDistance);
     menuBtn.center = CGPointMake(ScreenFrame.size.height/2, GetTopHeight+22);
    [menuBtn addTarget:self action:@selector(menuBtnAction) forControlEvents:UIControlEventTouchUpInside];
     [self.view addSubview:menuBtn];
 
    helper=[[WebServices alloc] initWithDelegate:self];
    page = 1;
    [self getHisDate];
 }
-(void)requestWithStationName:(NSString *)stationName deviceid:(NSString *)devid curArea:(NSString*)Area startTime:(NSString *)startTime endTime:(NSString *)endTime housSwitch:(BOOL)hour{
    
    curArea = Area;
    curdevid = devid;
    curstartTime = startTime;
    curendTime = endTime;
    curstationName = stationName;
    curhour = hour;
}
- (void)getHisDate{
    
    if (!dataArr) {
        dataArr = [NSMutableArray array];
    }
    [dataArr removeAllObjects];
    
    [AppHelper showHUD:@"正在获取换热站数据" inView:self.view];
    NSMutableArray *arr=[NSMutableArray array];
    //    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:curArea,@"areaID", nil]];
    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"20",@"pageSize", nil]];
    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%d",page],@"pageIndex", nil]];
    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"0",@"stationType", nil]];
    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:curstartTime,@"startTime", nil]];
    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:curendTime,@"endTime", nil]];
    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:curstationName,@"stationName", nil]];
    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:curhour?@"1":@"0",@"type", nil]];
    //    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:curdevid,@"deviceID", nil]];
    
    NSLog(@"%@",arr);
    
    NSString *soapMsg=[SoapHelper arrayToDefaultSoapMessage:arr methodName:@"HisDate"];
    [helper asyncServiceMethod:@"HisDate" soapMessage:soapMsg];
}
-(void)requestFinishedMessageForCount:(NSString *)count{
    NSInteger totoal = [count intValue]/20;
    if ([count intValue]%20!=0) {
        totoal++;
    }
    totoalPage = totoal;
//    totoalPagelb.text = [NSString stringWithFormat:@"共 %d 页",totoal];
}
#pragma mark -
#pragma mark WebServices delegate Methods
-(void)requestFinishedMessage:(NSString*)xml{
    [AppHelper removeHUD];
    NSData*data = [Base64 decodeBase64WithString:xml];
    NSError *error = nil;
    NSData*tempData = [ASIDataDecompressor uncompressData:data error:&error];
    
    NSStringEncoding gbkEncoding = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
    {
        NSString *xmlstr = [[NSString alloc] initWithData:tempData encoding:gbkEncoding];
        NSLog(@"返回的xml数据:%@",xmlstr);
        NSString*preSrt = [xmlstr componentsSeparatedByString:@"@"][0];
        NSLog(@"preSrt:%@",preSrt);
        NSString*sufSrt = [xmlstr componentsSeparatedByString:@"@"][1];
        
        if (!dataArr) {
            dataArr = [NSMutableArray array];
        }
        [dataArr removeAllObjects];
         dataArr = [[sufSrt JSONValue] mutableCopy];
        
        if (!OneGoTArr) {
            OneGoTArr = [NSMutableArray array];
        }
        [OneGoTArr removeAllObjects];
        if (!OneBackTArr) {
            OneBackTArr = [NSMutableArray array];
        }
        [OneBackTArr removeAllObjects];
        if (!OneGoPArr) {
            OneGoPArr = [NSMutableArray array];
        }
        [OneGoPArr removeAllObjects];
        if (!OneBackPArr) {
            OneBackPArr = [NSMutableArray array];
        }
        [OneBackPArr removeAllObjects];
        for (NSArray*ar in dataArr) {
            NSString*OneGoTStr = [NSString stringWithFormat:@"%f",[ar[2] floatValue]];
            [OneGoTArr addObject:OneGoTStr];
            
            NSString*OneBackTStr = [NSString stringWithFormat:@"%f",[ar[3] floatValue]];
            [OneBackTArr addObject:OneBackTStr];
            
            NSString*OneGoPStr = [NSString stringWithFormat:@"%f",[ar[4] floatValue]];
            [OneGoPArr addObject:OneGoPStr];
            
            NSString*OneBackPStr = [NSString stringWithFormat:@"%f",[ar[5] floatValue]];
            [OneBackPArr addObject:OneBackPStr];
         }
        {
            TTView = [[TempTrendView alloc] initWithFrame:CGRectMake(0, GetToTopDistance, ScreenFrame.size.height, self.view.frame.size.height-GetToTopDistance)];
            TTView.backgroundColor = [UIColor whiteColor];
            [self.view addSubview:TTView];
            [TTView setOneGoTempArr:OneGoTArr OneBackTempArr:OneBackTArr];
         }
        {
            PTView = [[PressTrendView alloc] initWithFrame:CGRectMake(0, GetToTopDistance, ScreenFrame.size.height, self.view.frame.size.height-GetToTopDistance)];
            PTView.backgroundColor = [UIColor whiteColor];
            [self.view addSubview:PTView];
            [PTView setOneGoPressArr:OneGoPArr OneBackPressArr:OneBackPArr];
            PTView.hidden = YES;
         }
    }
    
    if (error) {
        NSLog(@"%@",[error description]);
    }
}
-(void)requestFailedMessage:(NSError*)error{
    [AppHelper removeHUD];
    NSLog(@"请求失败:%@\n",[error description]);
}
-(BOOL)shouldAutorotate{
    return YES;
}
-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscapeRight;//这里写你需要的方向。看好了，别写错了
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
