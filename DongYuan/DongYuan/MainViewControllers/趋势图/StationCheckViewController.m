//
//  HistoryNormalStationViewController.m
//  DongYuan
//
//  Created by 鄢罗军 on 14/10/13.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import "StationCheckViewController.h"
#import "SoapHelper.h"
#import "ASIDataDecompressor.h"
#import "Base64.h"
#import "JSON.h"

#import "StationTrendViewController.h"
@interface StationCheckViewController ()

@end

@implementation StationCheckViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)backAction{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self AddCustomNavigationBar];
    [self AddCustomLeftImageItem:[UIImage imageNamed:@"返回"] WithSelector:@selector(backAction) Target:self];
    [self AddCustomNavigationbarTitle:@"热站趋势图查询"];
    {
        UILabel*line = [[UILabel alloc] initWithFrame:CGRectMake(0, GetToTopDistance+9.5, ScreenFrame.size.width, 0.5)];
        line.backgroundColor = [UIColor lightGrayColor];
        [self.view addSubview:line];
    }
    {
        UILabel*line = [[UILabel alloc] initWithFrame:CGRectMake(0, GetToTopDistance+35+10.5, ScreenFrame.size.width, 0.5)];
        line.backgroundColor = [UIColor lightGrayColor];
        [self.view addSubview:line];
    }
    {
        UILabel*line = [[UILabel alloc] initWithFrame:CGRectMake(ScreenFrame.size.width/2, GetToTopDistance+10, 0.5, 35)];
        line.backgroundColor = [UIColor lightGrayColor];
        [self.view addSubview:line];
    }
    
    UIView*bgview = [[UIView alloc] initWithFrame:CGRectMake(0, GetToTopDistance+10, ScreenFrame.size.width, 35)];
    [self.view addSubview:bgview];

    [self reCreatTableView];
    helper=[[WebServices alloc] initWithDelegate:self];
    
}
-(IBAction)ConfirmAction{
    if (curAreaArr.count == 0) {
        [UIAlertView ShowAlertViewWithMsg:@"请输入区域"];
        return;
    }
    if (!curStationName || [curStationName isEqualToString:@""]) {
        [UIAlertView ShowAlertViewWithMsg:@"请输入站点名称"];
        return;
    }
    if (curDeviceArr.count == 0) {
        [UIAlertView ShowAlertViewWithMsg:@"请输入设备"];
        return;
    }
    if (!startTime1 || [startTime1 isEqualToString:@""]) {
        [UIAlertView ShowAlertViewWithMsg:@"请输入开始时间"];
        return;
    }
    if (!endTime1 || [endTime1 isEqualToString:@""]) {
        [UIAlertView ShowAlertViewWithMsg:@"请输入结束时间"];
        return;
    }
    StationTrendViewController *ctrl = [[StationTrendViewController alloc] initWithNibName:@"StationTrendViewController" bundle:nil];
    [ctrl requestWithStationName:curStationName deviceid:[NSString stringWithFormat:@"%d",[curDeviceArr[0] intValue]] curArea:[NSString stringWithFormat:@"%d",[curAreaArr[0] intValue]] startTime:startTime1 endTime:endTime1 housSwitch:hourswitch];
    [self presentViewController:ctrl animated:NO completion:nil];
}
-(void)reCreatTableView{
    if (mainTable) {
        [mainTable removeFromSuperview];
        mainTable = nil;
    }
    mainTable = [[UITableView alloc] initWithFrame:CGRectMake(0, GetToTopDistance, ScreenFrame.size.width, 44*6) style:UITableViewStylePlain];
    mainTable.dataSource = self;
    mainTable.delegate = self;
    [self.view addSubview:mainTable];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 6;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString*cellid = @"cellid";
    UITableViewCell*cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellid];
        
            UILabel*lb = [[UILabel alloc] initWithFrame:CGRectMake(140, 5, 140, 34)];
            lb.backgroundColor = [UIColor clearColor];
            lb.font = [UIFont systemFontOfSize:14];
            [cell.contentView addSubview:lb];
            lb.tag = 10;
            lb.textAlignment = NSTextAlignmentCenter;
            
            if (indexPath.row == 5) {
                cell.textLabel.text = @"按小时计算";
                hourSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 50, 40)];
                cell.accessoryView = hourSwitch;
                [hourSwitch addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
            }
    }
        UILabel *lb = (UILabel *)[cell.contentView viewWithTag:10];
        if (indexPath.row == 0) {
            cell.textLabel.text = @"区域";
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            if (curAreaArr.count > 0) {
                lb.text = curAreaArr[1];
            }
        }
        if (indexPath.row == 1) {
            cell.textLabel.text = @"站点";
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            if (curStationName && ![curStationName isEqualToString:@""]) {
                lb.text = curStationName;
            }
        }
        if (indexPath.row == 2) {
            cell.textLabel.text = @"设备";
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            if (curDeviceArr.count > 0) {
                lb.text = curDeviceArr[1];
            }
        }
        if (indexPath.row == 3) {
            cell.textLabel.text = @"起始时间";
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            if (startTime1 && ![startTime1 isEqualToString:@""]) {
                lb.text = startTime1;
            }
        }
        if (indexPath.row == 4) {
            cell.textLabel.text = @"结束时间";
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            if (endTime1 && ![endTime1 isEqualToString:@""]) {
                lb.text = endTime1;
            }
        }
        if (indexPath.row == 5) {
            cell.textLabel.text = @"按小时计算";
            [hourSwitch setOn:hourswitch];
        }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
        if (indexPath.row == 0) {
            if (SharedDelegate.areaArrs.count == 0) {
                return;
            }
            selectAraeCtrl = [[SelecteAreaViewController alloc] initWithNibName:@"SelecteAreaViewController" bundle:nil];
            [self.view addSubview:selectAraeCtrl.view];
            [selectAraeCtrl setAreaArrs:SharedDelegate.areaArrs];
            selectAraeCtrl.delegate = self;
        }
        if (indexPath.row == 1) {
            if (curAreaArr.count == 0) {
                return;
            }
            [self getStationByAreaID:curAreaArr[0]];
        }
        if (indexPath.row == 2) {
            if (!curStationName || [curStationName isEqualToString:@""]) {
                return;
            }
            [self getDeviceByStationName];
        }
        if (indexPath.row == 3) {
            datepicker = [[MyDatePicker alloc] initWithFrame:CGRectMake(0, ScreenFrame.size.height, ScreenFrame.size.width, ScreenFrame.size.height)];
            datepicker.delegate = self;
            [[[UIApplication sharedApplication] keyWindow] addSubview:datepicker];
            [UIView animateWithDuration:0.3 animations:^{
                datepicker.frame = CGRectMake(0, 0, ScreenFrame.size.width, ScreenFrame.size.height);
            }];
            curDateIndex = 0;
        }
        if (indexPath.row == 4) {
            datepicker = [[MyDatePicker alloc] initWithFrame:CGRectMake(0, ScreenFrame.size.height, ScreenFrame.size.width, ScreenFrame.size.height)];
            datepicker.delegate = self;
            [[[UIApplication sharedApplication] keyWindow] addSubview:datepicker];
            [UIView animateWithDuration:0.3 animations:^{
                datepicker.frame = CGRectMake(0, 0, ScreenFrame.size.width, ScreenFrame.size.height);
            }];
            curDateIndex = 1;
    }
}
#pragma mark 时间控件 回调
-(void)MyDatePickerDidSelectedDate:(NSString *)dateString{
    if (!dateString) { //取消按钮
        [UIView animateWithDuration:0.1 animations:^{
            datepicker.frame = CGRectMake(0, ScreenFrame.size.height, ScreenFrame.size.width, ScreenFrame.size.height);
        }completion:^(BOOL finished){
            [datepicker removeFromSuperview];
            datepicker = nil;
        }];
        return;
    }
    if (dateString) {
        if (curDateIndex == 0) {
            startTime1 = dateString;
        }
        if (curDateIndex == 1) {
            endTime1 = dateString;
        }
        if (curDateIndex == 2) {
            startTime2 = dateString;
        }
        if (curDateIndex == 3) {
            endTime2 = dateString;
        }
        [mainTable reloadData];
        if (datepicker) {
            [UIView animateWithDuration:0.1 animations:^{
                
                datepicker.frame = CGRectMake(0, ScreenFrame.size.height, ScreenFrame.size.width, ScreenFrame.size.height);
                
            }completion:^(BOOL finished){
                
                [datepicker removeFromSuperview];
                datepicker = nil;
            }];
        }
    }
}

-(void)switchAction:(UISwitch *)sw{
    if (sw == hourSwitch) {
        hourswitch = sw.isOn;
    }
}
-(void)SelecteAreaConfirmBtnClicked:(NSArray *)selectedArr :(SelecteAreaViewController *)selectAreaCtrl{
    curAreaArr = [selectedArr mutableCopy];
    
    [mainTable reloadData];
}
- (void)getStationByAreaID:(NSString *)areaID{
    
    //    [AppHelper showHUD:@"正在获取换热站数据" inView:self.view];
    NSMutableArray *arr=[NSMutableArray array];
    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%d",[areaID intValue]],@"areaID", nil]];
    
    NSString *soapMsg=[SoapHelper arrayToDefaultSoapMessage:arr methodName:@"StationByAreaID"];
    [helper asyncServiceMethod:@"StationByAreaID" soapMessage:soapMsg];
}
- (void)getDeviceByStationName{
//    [AppHelper showHUD:@"正在获取换热站数据" inView:self.view];
    NSMutableArray *arr=[NSMutableArray array];
    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:curStationName?curStationName:@"",@"stationName", nil]];
    NSString *soapMsg=[SoapHelper arrayToDefaultSoapMessage:arr methodName:@"DeviceByStationName"];
    [helper asyncServiceMethod:@"DeviceByStationName" soapMessage:soapMsg];
    isGetDevice = YES;
}

#pragma mark -
#pragma mark WebServices delegate Methods
-(void)requestFinishedMessage:(NSString*)xml{
//    [AppHelper removeHUD];
    NSData*data = [Base64 decodeBase64WithString:xml];
    NSError *error = nil;
    NSData*tempData = [ASIDataDecompressor uncompressData:data error:&error];
    
    NSStringEncoding gbkEncoding = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
    {
        NSString *xmlstr = [[NSString alloc] initWithData:tempData encoding:gbkEncoding];
        NSLog(@"返回的xml数据:%@",xmlstr);
        NSString*preSrt = [xmlstr componentsSeparatedByString:@"@"][0];
        NSLog(@"preSrt:%@",preSrt);
        NSString*sufSrt = [xmlstr componentsSeparatedByString:@"@"][1];
        
        if (isGetDevice) {
            isGetDevice = NO;
            NSMutableArray*tempArr = [sufSrt JSONValue];
            selectDeviceCtrl= [[SelecteDeviceViewController alloc] initWithNibName:@"SelecteDeviceViewController" bundle:nil];
            [self.view addSubview:selectDeviceCtrl.view];
            [selectDeviceCtrl setDeviceArrs:tempArr];
            selectDeviceCtrl.delegate = self;
            return;
        }
        
        if (!stationNames) {
            stationNames = [NSMutableArray array];
        }
        [stationNames removeAllObjects];
        
        for (NSArray*ar in [sufSrt JSONValue]) {
            [stationNames addObject:ar[0]];
        }
        selectStationCtrl= [[SelecteStationViewController alloc] initWithNibName:@"SelecteStationViewController" bundle:nil];
        [self.view addSubview:selectStationCtrl.view];
        [selectStationCtrl setStationsArrs:stationNames];
        selectStationCtrl.delegate = self;
    }
    
    if (error) {
        NSLog(@"%@",[error description]);
    }
}
-(void)SelecteStationConfirmBtnClicked:(NSString *)selectedStation :(SelecteStationViewController *)selectAreaCtrl{
    curStationName = selectedStation;
    [mainTable reloadData];
}
-(void)SelecteDeviceConfirmBtnClicked:(NSArray *)selectedArr :(SelecteDeviceViewController *)selectAreaCtrl{
    curDeviceArr = (NSMutableArray *)selectedArr;
    [mainTable reloadData];
    
}
-(BOOL)shouldAutorotate{
    return YES;
}
-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
