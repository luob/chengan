//
//  HistoryNormalStationViewController.h
//  DongYuan
//
//  Created by 鄢罗军 on 14/10/13.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import "BASEUIViewController.h"
#import "WebServices.h"
#import "SelecteAreaViewController.h"
#import "SelecteStationViewController.h"
#import "SelecteDeviceViewController.h"
#import "MyDatePicker.h"

@interface StationCheckViewController : BASEUIViewController<UITableViewDataSource,UITableViewDelegate,WebServiceDelegate,SelecteAreaDelegate,SelecteStationDelegate,SelecteDeviceDelegate,DatePickerDelegate>
{
    UITableView*mainTable;
    NSMutableArray*dataArr;
    
    UISwitch*daySwitch,*weekSwitch,*monthSwitch;
    UISwitch*hourSwitch;
     
    WebServices *helper;
    NSMutableArray*stationNames;
    
    SelecteAreaViewController*selectAraeCtrl;
    SelecteStationViewController*selectStationCtrl;
    SelecteDeviceViewController*selectDeviceCtrl;
    
    NSMutableArray *curAreaArr;
    NSMutableArray *curDeviceArr;
    NSString*curStationName;
    
    BOOL isGetDevice;
    
    NSString *startTime1,*endTime1;
    NSString *startTime2,*endTime2;
    
    MyDatePicker *datepicker;
    
    NSInteger curDateIndex;
    
    BOOL hourswitch;
}
-(IBAction)ConfirmAction;
@end






