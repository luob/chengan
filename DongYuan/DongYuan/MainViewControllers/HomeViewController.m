//
//  HomeViewController.m
//  DongYuan
//
//  Created by 鄢罗军 on 14/9/19.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import "HomeViewController.h"
#import "SoapHelper.h"
#import "ASIDataDecompressor.h"
#import "Base64.h"
#import "JSON.h"

#import "HistoryCheckPointViewController.h"
@interface HomeViewController ()

@end

@implementation HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    [self AddCustomNavigationBar];
//     [self AddCustomNavigationbarTitle:@"津安热网监控"];
    
    [scroll setContentSize:CGSizeMake(375+75,75)];
    
    helper=[[WebServices alloc] initWithDelegate:self];
    [self getAreaNoData];
}
 -(IBAction)btnAction:(id)sender{
    NSInteger index = [(UIButton *)sender tag];
    switch (index) {
        case 0: //天气
        {
            WeatherViewController*ctrl = [[WeatherViewController alloc] initWithNibName:@"WeatherViewController" bundle:nil];
            [self.navigationController pushViewController:ctrl animated:YES];
         }
            break;
        case 1: //换热站
        {
            if (SharedDelegate.areaArrs.count == 0) {
                return;
            }
             selectAraeCtrl = [[SelecteAreaViewController alloc] initWithNibName:@"SelecteAreaViewController" bundle:nil];
            [self.view addSubview:selectAraeCtrl.view];
            [selectAraeCtrl setAreaArrs:SharedDelegate.areaArrs];
            selectAraeCtrl.delegate = self;
        }
            break;

        case 2: //关口
        {
            selectCheckPointCtrl = [[SelecteCheckPointViewController alloc] initWithNibName:@"SelecteCheckPointViewController" bundle:nil];
            [self.view addSubview:selectCheckPointCtrl.view];
            NSMutableArray *tempArr = [NSMutableArray array];
            [tempArr addObject:@"一级中继泵站"];
            [tempArr addObject:@"二级中继泵站"];
            [tempArr addObject:@"陈塘电厂"];
            [tempArr addObject:@"陈塘新厂"];
            
            [selectCheckPointCtrl setStationsArrs:tempArr];
            selectCheckPointCtrl.delegate = self;
        }
            break;

        case 3: //梅江
        {
            MJViewCtrl = [[MJViewController alloc] initWithNibName:@"MJViewController" bundle:nil];
            [self presentViewController:MJViewCtrl animated:NO completion:nil];

        }
            break;
        case 4:  //趋势图
        {
            selectStationAndCheckPointCtrl = [[SelecteStationAndCheckPointViewController alloc] initWithNibName:@"SelecteStationAndCheckPointViewController" bundle:nil];
            selectStationAndCheckPointCtrl.delegate = self;
            [self.view addSubview:selectStationAndCheckPointCtrl.view];
         }
            break;
        case 5:  //站点地图
        {
            if (SharedDelegate.areaArrs.count == 0) {
                return;
            }
            mapStationCtrl = [[SelecteAreaViewController alloc] initWithNibName:@"SelecteAreaViewController" bundle:nil];
            [self.view addSubview:mapStationCtrl.view];
            [mapStationCtrl setAreaArrs:SharedDelegate.areaArrs];
            mapStationCtrl.delegate = self;
         }
            break;
         default:
            break;
    }
}
-(void)SelecteStationBtnClicked:(SelecteStationAndCheckPointViewController *)selectAreaCtrl{
    StationCheckViewController*ctrl = [[StationCheckViewController alloc] initWithNibName:@"StationCheckViewController" bundle:nil];
    [self.navigationController pushViewController:ctrl animated:YES];
}
-(void)SelecteCheckPointBtnClicked:(SelecteStationAndCheckPointViewController *)selectAreaCtrl{
    CheckPointCheckViewController*seartchCtrl = [[CheckPointCheckViewController alloc] initWithNibName:@"CheckPointCheckViewController" bundle:nil];
    [self presentViewController:seartchCtrl animated:NO completion:nil];
}
-(void)SelecteCheckPointConfirmBtnClicked:(NSString *)selectedStation :(SelecteCheckPointViewController *)selectAreaCtrl{
    CheckPointViewController*ctrl = [[CheckPointViewController alloc] initWithNibName:@"CheckPointViewController" bundle:nil];
    [ctrl setCheckPointName:selectedStation];
    [self.navigationController pushViewController:ctrl animated:YES];
}

-(void)SelecteAreaConfirmBtnClicked:(NSArray *)selectedArr :(SelecteAreaViewController *)selectAreaCtrl{
    if (mapStationCtrl == selectAreaCtrl) {
        StationMapViewController*ctrl = [[StationMapViewController alloc] initWithNibName:@"StationMapViewController" bundle:nil];
        ctrl.areaArr = selectedArr;
        [self.navigationController pushViewController:ctrl animated:YES];
        return;
    }
    NormalHuanReStationViewController*ctrl = [[NormalHuanReStationViewController alloc] initWithNibName:@"NormalHuanReStationViewController" bundle:nil];
    ctrl.areaArr = selectedArr;
    [self presentViewController:ctrl animated:NO completion:nil];

}

- (void)getAreaNoData{
 
    NSMutableArray *arr=[NSMutableArray array];
     NSString *soapMsg=[SoapHelper arrayToDefaultSoapMessage:arr methodName:@"AreaNo_Select"];
    [helper asyncServiceMethod:@"AreaNo_Select" soapMessage:soapMsg];//WeatherNowData_Select
}

#pragma mark -
#pragma mark WebServices delegate Methods

-(void)requestFinishedMessage:(NSString*)xml{
    [AppHelper removeHUD];
    NSData*data = [Base64 decodeBase64WithString:xml];
    NSError *error = nil;
    NSData*tempData = [ASIDataDecompressor uncompressData:data error:&error];
    
    NSStringEncoding gbkEncoding = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
    {
        NSString *xmlstr = [[NSString alloc] initWithData:tempData encoding:gbkEncoding];
        NSLog(@"返回的xml数据:%@",xmlstr);
        NSString*preSrt = [xmlstr componentsSeparatedByString:@"@"][0];
        NSLog(@"preSrt:%@",preSrt);
        NSString*sufSrt = [xmlstr componentsSeparatedByString:@"@"][1];
        SharedDelegate.areaArrs =  [[sufSrt JSONValue] mutableCopy];
    }
    
    if (error) {
        NSLog(@"%@",[error description]);
    }
    /*["CJTime","SJTime","WeatherID","Temp","WD","WS","SD","Weather","Image","TempSize","Time"]
     @[["2014-09-28T19:05:00.363","2014-09-28T17:00:00",101030100,23.0,"东南风","0.8",null,"多云转阴","d1.gif","25℃~16℃","9月28日"]]
     */
}
-(void)requestFailedMessage:(NSError*)error{
    [AppHelper removeHUD];
    NSLog(@"请求失败:%@\n",[error description]);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
