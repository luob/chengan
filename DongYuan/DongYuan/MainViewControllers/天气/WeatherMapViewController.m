//
//  WeatherMapViewController.m
//  DongYuan
//
//  Created by 鄢罗军 on 14/10/14.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import "WeatherMapViewController.h"

#import "PNLineChartView.h"
#import "PNPlot.h"
@interface WeatherMapViewController ()

@end

@implementation WeatherMapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)backAction{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self AddCustomNavigationBar];
    [self AddCustomLeftImageItem:[UIImage imageNamed:@"返回"] WithSelector:@selector(backAction) Target:self];
    [self AddCustomNavigationbarTitle:@"天气图"];
    // Do any additional setup after loading the view from its nib.
 
    
    PNLineChartView *lineChartView = [[PNLineChartView alloc] initWithFrame:CGRectMake(0, GetToTopDistance, ScreenFrame.size.width, 300)];
    lineChartView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:lineChartView];
    
    UILabel*titlelb = [[UILabel alloc] initWithFrame:CGRectMake(20, GetToTopDistance, 100, 30)];
    titlelb.text = @"单位:℃";
    titlelb.backgroundColor = [UIColor clearColor];
    titlelb.font = [UIFont boldSystemFontOfSize:16];
    [self.view addSubview:titlelb];
    {
        UILabel*lb = [[UILabel alloc] initWithFrame:CGRectMake(15, GetToTopDistance+292, 50, 15)];
        lb.text = @"--最高温度";
        lb.backgroundColor = [UIColor clearColor];
        lb.textColor = [UIColor redColor];
        lb.font = [UIFont systemFontOfSize:10];
         [self.view addSubview:lb];
    }
    {
        UILabel*lb = [[UILabel alloc] initWithFrame:CGRectMake(15+60, GetToTopDistance+292, 100, 15)];
        lb.text = @"--最低温度";
        lb.backgroundColor = [UIColor clearColor];
        lb.textColor = [UIColor greenColor];
        lb.font = [UIFont systemFontOfSize:10];
        [self.view addSubview:lb];
     }
    
    NSMutableArray*highAr = [NSMutableArray array];
    NSMutableArray*lowAr = [NSMutableArray array];

    for (NSArray*ar in dataArr) {
        NSArray*tempAr = [ar[6] componentsSeparatedByString:@"~"];
        [highAr addObject:[tempAr[0] componentsSeparatedByString:@"℃"][0]];
        [lowAr addObject:[tempAr[1] componentsSeparatedByString:@"℃"][0]];
    }
    
    lineChartView.max = 40;
    lineChartView.min = -20;
    
    lineChartView.interval = (lineChartView.max-lineChartView.min)/12;
    
    NSMutableArray* yAxisValues = [@[] mutableCopy];
    for (int i=0; i<13; i++) {
        NSString* str = [NSString stringWithFormat:@"%.2f", lineChartView.min+lineChartView.interval*i];
        [yAxisValues addObject:str];
    }
     lineChartView.xAxisValues = @[@"1", @"2", @"3",@"4", @"5", @"6"];
    lineChartView.yAxisValues = yAxisValues;
    lineChartView.axisLeftLineWidth = 30;
    
    PNPlot *plot1 = [[PNPlot alloc] init];
    plot1.plottingValues = highAr;
     plot1.lineColor = [UIColor redColor];
    plot1.lineWidth = 0.5;
     [lineChartView addPlot:plot1];
 
    PNPlot *plot2 = [[PNPlot alloc] init];
     plot2.plottingValues = lowAr;
     plot2.lineColor = [UIColor greenColor];
    plot2.lineWidth = 0.5;
     [lineChartView  addPlot:plot2];
    
    mainTable = [[UITableView alloc] initWithFrame:CGRectMake(0, GetToTopDistance+310, self.view.frame.size.width, self.view.frame.size.height-(GetToTopDistance+310)) style:UITableViewStylePlain];
    mainTable.dataSource = self;
    mainTable.delegate = self;
    [self.view addSubview:mainTable];
}
-(void)setDataArr:(NSMutableArray*)arr{
    if (!dataArr) {
        dataArr = [NSMutableArray array];
    }
    dataArr = [arr mutableCopy];
    [mainTable reloadData];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArr.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cellid = @"123";
    UITableViewCell*cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellid];
        
        UIFont *font = [UIFont systemFontOfSize:14];
        
        UILabel*datelb=[[UILabel alloc] initWithFrame:CGRectMake(5, 10, 75, 30)];
        datelb.tag = 10;
        datelb.font = font;
        [cell.contentView addSubview:datelb];
        
        UILabel*weatherlb=[[UILabel alloc] initWithFrame:CGRectMake(80, 10, 80, 30)];
        weatherlb.tag = 11;
        weatherlb.textAlignment = NSTextAlignmentCenter;
        [cell.contentView addSubview:weatherlb];
        weatherlb.font = font;
        
        UILabel*templb=[[UILabel alloc] initWithFrame:CGRectMake(160, 10, 80, 30)];
        templb.tag = 12;
        [cell.contentView addSubview:templb];
        templb.font = font;
        
        UIImageView*imagev = [[UIImageView alloc] initWithFrame:CGRectMake(255, 8, 35, 35)];
        imagev.tag = 13;
        [cell.contentView addSubview:imagev];
    }
    
    NSArray*curArr = [dataArr objectAtIndex:indexPath.row];
    UILabel*datelb=(UILabel *)[cell.contentView viewWithTag:10];
    NSDateFormatter*ff = [[NSDateFormatter alloc] init];
    [ff setDateFormat:@"MM月dd日"];
    datelb.text= [ff stringFromDate:[NSDate dateWithTimeInterval:24*60*60*indexPath.row sinceDate:[NSDate date]]];
    
    UILabel*weatherlb=(UILabel *)[cell.contentView viewWithTag:11];
    weatherlb.text = curArr[4];
    
    UILabel*templb=(UILabel *)[cell.contentView viewWithTag:12];
    templb.text = curArr[6];
    
    UIImageView*imagev = (UIImageView*)[cell.contentView viewWithTag:13];
    NSString*imagename = curArr[4];
    if ([imagename isEqualToString:@"霾转多云"]) {
        imagename = @"阴转多云";
    }
    imagev.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",imagename]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
