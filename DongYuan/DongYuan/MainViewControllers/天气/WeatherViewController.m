//
//  WeatherViewController.m
//  DongYuan
//
//  Created by 鄢罗军 on 14/9/28.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import "WeatherViewController.h"
#import "SoapHelper.h"
#import "ASIDataDecompressor.h"
#import "Base64.h"
#import "JSON.h"

#import "WeatherMapViewController.h"
@interface WeatherViewController ()

@end

@implementation WeatherViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)weatherMapBtnAction{
    WeatherMapViewController*ctrl = [[WeatherMapViewController alloc] initWithNibName:@"WeatherMapViewController" bundle:nil];
    [ctrl setDataArr:dataArr];
    [self.navigationController pushViewController:ctrl animated:YES];
}
-(void)backAction{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self AddCustomNavigationBar];
    [self AddCustomLeftImageItem:[UIImage imageNamed:@"返回"] WithSelector:@selector(backAction) Target:self];
    [self AddCustomNavigationbarTitle:@"天气情况"];
    
    
    UIButton *historyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    historyBtn.frame = CGRectMake(self.view.frame.size.width-70, GetTopHeight, 60, 44);
    [historyBtn addTarget:self action:@selector(weatherMapBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [historyBtn setTitle:@"天气图" forState:UIControlStateNormal];
    [historyBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:historyBtn];
    
    UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(0, GetToTopDistance, 320, 60)];
    image.image = [[UIImage imageNamed:@"home_01.png"] stretchableImageWithLeftCapWidth:2 topCapHeight:10];
    [self.view addSubview:image];
    
    toDayTemplb = [[UILabel alloc] initWithFrame:CGRectMake(15, 5, 100, 32)];
    toDayTemplb.font = [UIFont boldSystemFontOfSize:20];
    toDayTemplb.textColor = [UIColor whiteColor];
//    [image addSubview:toDayTemplb];
    
    toDayDatelb = [[UILabel alloc] initWithFrame:CGRectMake(15, 35, 80, 28)];
    toDayDatelb.font = [UIFont systemFontOfSize:16];
    toDayDatelb.textColor = [UIColor whiteColor];
    [image addSubview:toDayDatelb];
    NSDateFormatter*ff = [[NSDateFormatter alloc] init];
    [ff setDateFormat:@"MM月dd日"];
    toDayDatelb.text = [ff stringFromDate:[NSDate date]];
    
    toDayTempSizelb = [[UILabel alloc] initWithFrame:CGRectMake(15, 5, 100, 32)];
    toDayTempSizelb.font = [UIFont boldSystemFontOfSize:18];
    toDayTempSizelb.textColor = [UIColor whiteColor];
    [image addSubview:toDayTempSizelb];
    
    todayImage = [[UIImageView alloc] initWithFrame:CGRectMake(260, 20, 35, 35)];
    [image addSubview:todayImage];
    
    
    mainTable = [[UITableView alloc] initWithFrame:CGRectMake(0, GetToTopDistance+60, self.view.frame.size.width, self.view.frame.size.height-(GetToTopDistance+60)) style:UITableViewStylePlain];
    mainTable.dataSource = self;
    mainTable.delegate = self;
    [self.view addSubview:mainTable];
    
    
    isGetTodayWeather = NO;
    isGetWeekWeather = NO;
    
    helper=[[WebServices alloc] initWithDelegate:self];
    
//    [self getTodayWeatherData];
     [self getWeekWeatherData];

}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArr.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cellid = @"123";
    UITableViewCell*cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellid];
  
        UIFont *font = [UIFont systemFontOfSize:14];
        
        UILabel*datelb=[[UILabel alloc] initWithFrame:CGRectMake(5, 10, 75, 30)];
        datelb.tag = 10;
        datelb.font = font;
        [cell.contentView addSubview:datelb];

        UILabel*weatherlb=[[UILabel alloc] initWithFrame:CGRectMake(80, 10, 80, 30)];
        weatherlb.tag = 11;
        weatherlb.textAlignment = NSTextAlignmentCenter;
        [cell.contentView addSubview:weatherlb];
        weatherlb.font = font;

        UILabel*templb=[[UILabel alloc] initWithFrame:CGRectMake(160, 10, 80, 30)];
        templb.tag = 12;
        [cell.contentView addSubview:templb];
        templb.font = font;
        
        UIImageView*imagev = [[UIImageView alloc] initWithFrame:CGRectMake(255, 8, 35, 35)];
        imagev.tag = 13;
        [cell.contentView addSubview:imagev];
    }
    
    NSArray*curArr = [dataArr objectAtIndex:indexPath.row];
    UILabel*datelb=(UILabel *)[cell.contentView viewWithTag:10];
    NSDateFormatter*ff = [[NSDateFormatter alloc] init];
    [ff setDateFormat:@"MM月dd日"];
    datelb.text= [ff stringFromDate:[NSDate dateWithTimeInterval:24*60*60*indexPath.row sinceDate:[NSDate date]]];
    
    UILabel*weatherlb=(UILabel *)[cell.contentView viewWithTag:11];
    weatherlb.text = curArr[4];
    
    UILabel*templb=(UILabel *)[cell.contentView viewWithTag:12];
    templb.text = curArr[6];
    
    UIImageView*imagev = (UIImageView*)[cell.contentView viewWithTag:13];
    NSString*imagename = curArr[4];
    if ([imagename isEqualToString:@"霾转多云"]) {
        imagename = @"阴转多云";
    }
    NSRange rang = [imagename rangeOfString:@"多云"];
    if (rang.location != NSNotFound) {
        imagename = @"多云"; //包含多云字符的全部用多云的图标
    }
    imagev.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",imagename]];
     return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)getTodayWeatherData{
    
    isGetTodayWeather = YES;
    
    [AppHelper showHUD:@"正在获取今日数据"];
    NSMutableArray *arr=[NSMutableArray array];
    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:kWeatherID,@"weatherID", nil]];
    
    NSString *soapMsg=[SoapHelper arrayToDefaultSoapMessage:arr methodName:@"WeatherNowData_Select"];
    [helper asyncServiceMethod:@"WeatherNowData_Select" soapMessage:soapMsg];//WeatherNowData_Select
}
- (void)getWeekWeatherData{
    
    isGetWeekWeather = YES;
    
    [AppHelper showHUD:@"正在获取未来七天数据"];
     NSMutableArray *arr=[NSMutableArray array];
    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:kWeatherID,@"weatherID", nil]];

    NSString *soapMsg=[SoapHelper arrayToDefaultSoapMessage:arr methodName:@"WeatherWeek_Select"];
    [helper asyncServiceMethod:@"WeatherWeek_Select" soapMessage:soapMsg];//WeatherNowData_Select
}

#pragma mark -
#pragma mark WebServices delegate Methods
-(void)requestFinishedMessage:(NSString*)xml{
    [AppHelper removeHUD];
    NSData*data = [Base64 decodeBase64WithString:xml];
    NSError *error = nil;
    NSData*tempData = [ASIDataDecompressor uncompressData:data error:&error];
    
    NSStringEncoding gbkEncoding = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
    {
        NSString *xmlstr = [[NSString alloc] initWithData:tempData encoding:gbkEncoding];
        NSLog(@"返回的xml数据:%@",xmlstr);
        NSString*preSrt = [xmlstr componentsSeparatedByString:@"@"][0];
        NSLog(@"preSrt:%@",preSrt);
         NSString*sufSrt = [xmlstr componentsSeparatedByString:@"@"][1];
        NSArray *result = [sufSrt JSONValue];
        if ([result count] <= 0) {
            [UIAlertView ShowAlertViewWithMsg:@"数据为空"];
            return;
        }
        if (isGetTodayWeather) {
            isGetTodayWeather = NO;
            toDayTemplb.text = [NSString stringWithFormat:@"%.01f ℃",[[result[0] objectAtIndex:3] floatValue]];
            toDayTempSizelb.text = [result[0] objectAtIndex:9];
            [self getWeekWeatherData];
            return;
        }
   
        if (isGetWeekWeather) {
            if (!dataArr) {
                dataArr = [NSMutableArray array];
            }
            [dataArr removeAllObjects];
            [dataArr addObjectsFromArray:[sufSrt JSONValue]];
            
            toDayTemplb.text = [NSString stringWithFormat:@"%.01f ℃",[[dataArr[0] objectAtIndex:6] floatValue]];
            toDayTempSizelb.text = [dataArr[0] objectAtIndex:6];
            NSString*imagename = dataArr[0][4];
             todayImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",imagename]];
               [mainTable reloadData];

        }
    }
    if (error) {
        NSLog(@"%@",[error description]);
    }
/*["CJTime","SJTime","WeatherID","Temp","WD","WS","SD","Weather","Image","TempSize","Time"]
 @[["2014-09-28T19:05:00.363","2014-09-28T17:00:00",101030100,23.0,"东南风","0.8",null,"多云转阴","d1.gif","25℃~16℃","9月28日"]]
*/
}
-(void)requestFailedMessage:(NSError*)error{
    [AppHelper removeHUD];
    NSLog(@"请求失败:%@\n",[error description]);
 }
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
