//
//  WeatherViewController.h
//  DongYuan
//
//  Created by 鄢罗军 on 14/9/28.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import "BASEUIViewController.h"
#import "WebServices.h"


@interface WeatherViewController : BASEUIViewController<WebServiceDelegate,UITableViewDataSource,UITableViewDelegate>{
    WebServices *helper;
    
    UITableView *mainTable;
    NSMutableArray*dataArr;
    
    UILabel*toDayTemplb,*toDayDatelb,*toDayTempSizelb;
    UIImageView*todayImage;
    
    BOOL isGetTodayWeather,isGetWeekWeather;
}


@end
