//
//  WeatherMapViewController.h
//  DongYuan
//
//  Created by 鄢罗军 on 14/10/14.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import "BASEUIViewController.h"

@interface WeatherMapViewController : BASEUIViewController<UITableViewDataSource,UITableViewDelegate>{
    UITableView *mainTable;
    NSMutableArray*dataArr;
}
-(void)setDataArr:(NSMutableArray*)arr;
@end
