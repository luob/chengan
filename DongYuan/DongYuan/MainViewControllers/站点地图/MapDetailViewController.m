//
//  MapDetailViewController.m
//  DongYuan
//
//  Created by 鄢罗军 on 14/10/13.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import "MapDetailViewController.h"

@interface MapDetailViewController ()

@end

@implementation MapDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)backAction{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self AddCustomNavigationBar];
    [self AddCustomLeftImageItem:[UIImage imageNamed:@"返回"] WithSelector:@selector(backAction) Target:self];
    [self AddCustomNavigationbarTitle:@"站点地图"];
    
    mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0.0, GetToTopDistance, ScreenFrame.size.width, [UIScreen mainScreen].bounds.size.height-(GetToTopDistance))];
    [self.view addSubview:mapView];
    mapView.showsUserLocation = YES;
    mapView.delegate = self;
    
    NSMutableArray*annotations = [NSMutableArray array];
    
    for (NSArray*ar in mapDatas) {
        NSString *coorsStr = ar[1];
        NSArray *coordArr = [coorsStr componentsSeparatedByString:@","];
        if (coordArr.count < 2) {
            coordArr = [coorsStr componentsSeparatedByString:@"@"];
        }
        if (coordArr.count < 2) {
            continue;
        }
        
        double lat,lon=0;

        double la = [coordArr [0] doubleValue];
        double lo = [coordArr [1] doubleValue];
        if (la > 100) {
            lat = lo;
            lon = la;
        }else{
            lat = la;
            lon = lo;
        }
        NSLog(@"维度：%lf",lat);
        NSLog(@"经度：%lf",lon);

        [annotations addObject:[self showAnnotationWithLat:lat lon:lon title:ar[0] des:ar[2]]];
    }
    [mapView addAnnotations:annotations];
    [mapView setSelectedAnnotations:annotations];

}
-(void)setAnnotationsWithCoordnations:(NSMutableArray *)coords{
    if (!mapDatas) {
        mapDatas = [NSMutableArray array];
    }
    mapDatas = [coords mutableCopy];
}
-(MKPointAnnotation *)showAnnotationWithLat:(double)lat lon:(double)lon title:(NSString *)title des:(NSString *)des{
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    annotation.coordinate = CLLocationCoordinate2DMake(lat, lon);
    annotation.title = title;
    annotation.subtitle = @"";
    // 指定新的显示区域
    [mapView setRegion:MKCoordinateRegionMake(annotation.coordinate, MKCoordinateSpanMake(0.02, 0.02)) animated:YES];
    return annotation;
}
// 根据anntation生成对应的View
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation{
    static NSString *placemarkIdentifier = @"my annotation identifier";
    if ([annotation isKindOfClass:[MKAnnotationView class]])
    {
        MKAnnotationView *annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:placemarkIdentifier];
        if (annotationView == nil)
        {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:placemarkIdentifier];
        }
        else
            annotationView.annotation = annotation;//这句看不懂
        return annotationView;
    }
    return nil;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end








