//
//  StationMapDetailViewController.m
//  DongYuan
//
//  Created by 鄢罗军 on 14/10/11.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import "StationMapDetailViewController.h"
#import "Base64.h"

@interface StationMapDetailViewController ()

@end

@implementation StationMapDetailViewController
@synthesize staionArr;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(IBAction)backBtnAction{
    [self backAction];
}
-(void)backAction{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
     
    [self AddCustomNavigationBar];
    [self AddCustomLeftImageItem:[UIImage imageNamed:@"返回"] WithSelector:@selector(backAction) Target:self];
    [self AddCustomNavigationbarTitle:@"站点地图"];
    // Do any additional setup after loading the view from its nib.
    
    UILabel*lb= [[UILabel alloc] initWithFrame:CGRectMake(0, GetToTopDistance+15, ScreenFrame.size.width, 30)];
    lb.textAlignment = NSTextAlignmentCenter;
    lb.font = [UIFont systemFontOfSize:16];
    [self.view addSubview:lb];
    lb.backgroundColor = [UIColor clearColor];
    lb.text = staionArr[0];
    
    UIImageView*imagev = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 260, 260)];
    imagev.center = CGPointMake(ScreenFrame.size.width/2, GetToTopDistance+130+70);
    [self.view addSubview:imagev];
    
    UIImage *image = [UIImage imageWithData:[Base64 decodeBase64WithString:staionArr[2]]];
    imagev.image = image;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
