//
//  StationMapDetail2ViewController.m
//  DongYuan
//
//  Created by 鄢罗军 on 14/10/11.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import "StationMapDetail2ViewController.h"
#import "SoapHelper.h"
#import "ASIDataDecompressor.h"
#import "Base64.h"
#import "JSON.h"
@interface StationMapDetail2ViewController ()

@end

@implementation StationMapDetail2ViewController
@synthesize staionArr;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)backAction{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self AddCustomNavigationBar];
    [self AddCustomLeftImageItem:[UIImage imageNamed:@"返回"] WithSelector:@selector(backAction) Target:self];
    [self AddCustomNavigationbarTitle:@"站点地图"];
    
    UILabel*lb= [[UILabel alloc] initWithFrame:CGRectMake(0, GetToTopDistance+15, ScreenFrame.size.width, 30)];
    lb.textAlignment = NSTextAlignmentCenter;
    lb.font = [UIFont systemFontOfSize:16];
    [self.view addSubview:lb];
    lb.backgroundColor = [UIColor clearColor];
    lb.text = staionArr[0];
    
    imageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [imageBtn setImage:[UIImage imageNamed:@"pic-bg1_03"] forState:UIControlStateNormal];
    [imageBtn setFrame:CGRectMake(0, 0, 120, 120)];
    imageBtn.center = CGPointMake(ScreenFrame.size.width/2, GetToTopDistance+70+60);
    [imageBtn addTarget:self action:@selector(btnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:imageBtn];
    
    {
        UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(10, GetToTopDistance+70+120, 200, 30)];
        lb.text = @"最多可以上传一张照片";
        lb.backgroundColor = [UIColor clearColor];
        lb.font = [UIFont systemFontOfSize:15];
        lb.textColor = [UIColor grayColor];
        [self.view addSubview:lb];
    }
    
    UIImageView*imagev = [[UIImageView alloc] initWithFrame:CGRectMake(10, GetToTopDistance+70+160, 290, 43)];
    imagev.image = [[UIImage imageNamed:@"map_07"] stretchableImageWithLeftCapWidth:100 topCapHeight:30];
    [self.view addSubview:imagev];
    
    addresslb = [[UILabel alloc] initWithFrame:CGRectMake(30, 5, 260, 33)];
    [imagev addSubview:addresslb];
    addresslb.backgroundColor = [UIColor clearColor];
    addresslb.font = [UIFont systemFontOfSize:14];
    addresslb.textColor = [UIColor whiteColor];
    addresslb.adjustsFontSizeToFitWidth = YES;
    {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setBackgroundColor:[UIColor ColorWithHexString:@"d63d3d"]];
        [btn setTitle:@"取消" forState:UIControlStateNormal];
         [btn setFrame:CGRectMake(0, 0, 290, 50)];
        btn.center = CGPointMake(ScreenFrame.size.width/2, GetToTopDistance+70+160+90);
        [btn addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
    }
    {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setBackgroundColor:[UIColor ColorWithHexString:@"3db334"]];
        [btn setTitle:@"确定" forState:UIControlStateNormal];
        [btn setFrame:CGRectMake(0, 0, 290, 50)];
        btn.center = CGPointMake(ScreenFrame.size.width/2, GetToTopDistance+70+160+50+100);
        [btn addTarget:self action:@selector(confirmAction) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
    }
    
    manager = [[CLLocationManager alloc] init];
    manager.delegate = self;
    [manager startUpdatingLocation];
    
    helper=[[WebServices alloc] initWithDelegate:self];

}
#pragma mark CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation{
    
    lat = manager.location.coordinate.latitude;
    lon = manager.location.coordinate.longitude;
    
    CLGeocoder *geoCoder = [[CLGeocoder alloc]init];
    //Geocoding Block
    [geoCoder reverseGeocodeLocation: manager.location completionHandler:
     ^(NSArray *placemarks, NSError *error) {
         //Get nearby address
         CLPlacemark *placemark = [placemarks objectAtIndex:0];

         //完整地址信息
         NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
         
//         if (locatedAt && ![locatedAt isEqualToString:@""]) {
//             addresslb.text = locatedAt;
//         }
         BOOL failed = NO;
         NSString *subThoroughfare=placemark.subThoroughfare;
         if (!placemark.locality || [placemark.locality isEqual:[NSNull null]] || [placemark.locality isEqualToString:@"(null)"]) {
             failed = YES;
         }
         if (!placemark.subLocality || [placemark.subLocality isEqual:[NSNull null]] || [placemark.subLocality isEqualToString:@"(null)"]) {
             failed = YES;
         }
         if (!placemark.subThoroughfare || [placemark.subThoroughfare isEqual:[NSNull null]] || [placemark.subThoroughfare isEqualToString:@"(null)"]) {
             failed = YES;
         }
         if (failed) {
             [UIAlertView ShowAlertViewWithMsg:@"定位失败，请返回重试"];
         }else{
             addresslb.text = [NSString stringWithFormat:@"%@%@%@%@",placemark.locality,placemark.subLocality,placemark.thoroughfare,subThoroughfare];
         }
         NSLog(@"%@",placemark.locality); //南京市
         NSLog(@"%@",placemark.subLocality); //雨花台区
         NSLog(@"%@",placemark.thoroughfare); //长虹路
         NSLog(@"%@",placemark.subThoroughfare); //437号

     }];
    [manager stopUpdatingLocation];
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    [manager stopUpdatingLocation];
    [UIAlertView ShowAlertViewWithMsg:@"定位失败"];
}

-(void)btnAction{
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"相册",nil];
    [sheet showInView:[[UIApplication sharedApplication] keyWindow]];
}
-(void)cancelAction{
    [self backAction];
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
//    if (buttonIndex == 0) {//拍照
//        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera
//             ]){
//            
//            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
//            imagePicker.delegate = self;
//            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
//            imagePicker.allowsEditing = YES;
//            [self presentModalViewController:imagePicker animated:YES];
//         }
//        else{
//            
//            [UIAlertView ShowAlertViewWithMsg:@"打开照相机失败"];
//        }
//        
//    }else
        if(buttonIndex == 0){ //相册
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary
             ]){
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
            imagePicker.delegate = self;
            imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            //            imagePicker.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
            //调用系统的裁剪
            imagePicker.allowsEditing = YES;
            
            [self presentModalViewController:imagePicker animated:YES];
        }
        else{
            [UIAlertView ShowAlertViewWithMsg:@"打开相册失败"];
         }
    }
}

#pragma mark - UIImagePickerDelegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo{
    
    [picker dismissModalViewControllerAnimated:NO];
    picker = nil;
    
    if (image) {
        selectedImage = image;
        [imageBtn setImage:image forState:UIControlStateNormal];
     }
}
-(void)confirmAction{
    
    if (!selectedImage) {
        [UIAlertView ShowAlertViewWithMsg:@"请选择照片"];
        return;
    }
    
    [self StationMapInfoUpdate];
    
    /*
     Int StationMapInfoUpdate(string stationName, string coordinate,byte[] image,string descInfo)
     作用	根据换热站名称更新换热站地图数据
     参数	stationName	站点名称
     coordinate	站点坐标  例如：100,200
     image	图片
     descInfo	地理位置描述
     */
}
- (void)StationMapInfoUpdate{
    
    [AppHelper showHUD:@"请稍后" inView:self.view];
    NSMutableArray *arr=[NSMutableArray array];
    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:staionArr[0],@"stationName", nil]];
    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%f@%f",lon,lat],@"coordinate", nil]];
    
    NSString *imageStr = [Base64 encodeBase64WithData:UIImageJPEGRepresentation(selectedImage, 0)];
    
    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:imageStr,@"image", nil]];
    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:addresslb.text,@"descInfo", nil]];

    NSString *soapMsg=[SoapHelper arrayToDefaultSoapMessage:arr methodName:@"StationMapInfoUpdate"];
    [helper asyncServiceMethod:@"StationMapInfoUpdate" soapMessage:soapMsg];
}
 
#pragma mark -
#pragma mark WebServices delegate Methods
-(void)requestFinishedMessage:(NSString*)xml{
    [AppHelper removeHUD];
    NSData*data = [Base64 decodeBase64WithString:xml];
    NSError *error = nil;
    NSData*tempData = [ASIDataDecompressor uncompressData:data error:&error];
    
    NSStringEncoding gbkEncoding = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
    {
        NSString *xmlstr = [[NSString alloc] initWithData:tempData encoding:gbkEncoding];
        NSLog(@"返回的xml数据:%@",xmlstr);
        NSString*preSrt = [xmlstr componentsSeparatedByString:@"@"][0];
        NSLog(@"preSrt:%@",preSrt);
//        NSString*sufSrt = [xmlstr substringFromIndex:preSrt.length+1];
            }
    
    if (error) {
        NSLog(@"%@",[error description]);
    }
}
-(void)requestFailedMessage:(NSError*)error{
    [AppHelper removeHUD];
    NSLog(@"请求失败:%@\n",[error description]);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end







