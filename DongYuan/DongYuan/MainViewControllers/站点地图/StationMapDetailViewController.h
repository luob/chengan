//
//  StationMapDetailViewController.h
//  DongYuan
//
//  Created by 鄢罗军 on 14/10/11.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import "BASEUIViewController.h"

@interface StationMapDetailViewController : BASEUIViewController

@property(nonatomic,retain)NSArray*staionArr;

-(IBAction)backBtnAction;

@end
