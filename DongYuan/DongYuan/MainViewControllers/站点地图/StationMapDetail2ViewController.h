//
//  StationMapDetail2ViewController.h
//  DongYuan
//
//  Created by 鄢罗军 on 14/10/11.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import "BASEUIViewController.h"

#import <CoreLocation/CoreLocation.h>
#import "WebServices.h"
@interface StationMapDetail2ViewController : BASEUIViewController<CLLocationManagerDelegate,WebServiceDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate>{
    UILabel*addresslb;
    
    CLLocationManager*manager;
    WebServices *helper;
    
    float lon,lat;
    
    UIImage*selectedImage;
    
    UIButton*imageBtn;

}
@property(nonatomic,retain)NSArray*staionArr;

@end
