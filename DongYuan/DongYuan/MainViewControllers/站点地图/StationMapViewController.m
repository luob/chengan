//
//  StationMapViewController.m
//  DongYuan
//
//  Created by 鄢罗军 on 14/10/9.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import "StationMapViewController.h"
#import "SoapHelper.h"
#import "ASIDataDecompressor.h"
#import "Base64.h"
#import "JSON.h"

#import "StationMapDetailViewController.h"
#import "StationMapDetail2ViewController.h"

#import "MapDetailViewController.h"

@interface StationMapViewController ()

@end

@implementation StationMapViewController
@synthesize areaArr;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)mapBtnAction{
    MapDetailViewController *ctrl = [[MapDetailViewController alloc] initWithNibName:@"MapDetailViewController" bundle:nil];
    [ctrl setAnnotationsWithCoordnations:didMarkArr];
    [self.navigationController pushViewController:ctrl animated:YES];
}
-(void)backAction{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self AddCustomNavigationBar];
    [self AddCustomLeftImageItem:[UIImage imageNamed:@"返回"] WithSelector:@selector(backAction) Target:self];
     [self AddCustomNavigationbarTitle:@"站点地图"];
    
    UIButton *searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    searchBtn.frame = CGRectMake(self.view.frame.size.width-60, GetTopHeight, 50, 44);
    [searchBtn addTarget:self action:@selector(mapBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [searchBtn setTitle:@"地图" forState:UIControlStateNormal];
    [searchBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:searchBtn];

    
    segControl = [[UISegmentedControl alloc] initWithFrame:CGRectMake(10, GetToTopDistance+15, ScreenFrame.size.width-20, 30)];
    [segControl insertSegmentWithTitle:@"全部" atIndex:0 animated:YES];
    [segControl insertSegmentWithTitle:@"已标记" atIndex:1 animated:YES];
    [segControl insertSegmentWithTitle:@"未标记" atIndex:2 animated:YES];
    [segControl insertSegmentWithTitle:@"照片" atIndex:3 animated:YES];
    [self.view addSubview:segControl];
    
    [segControl addTarget:self action:@selector(segControlAction) forControlEvents:UIControlEventValueChanged];
    
    segControl.selectedSegmentIndex = 0;
    
    helper=[[WebServices alloc] initWithDelegate:self];
    page = 1;
    
    [self reCreatMainTable];
    
    [self getStationMapInfoSelectByArea];

}
-(void)reCreatMainTable{
    if (maintable) {
        [maintable removeFromSuperview];
        maintable = nil;
    }
    maintable = [[UITableView alloc] initWithFrame:CGRectMake(0, GetToTopDistance+15+30, ScreenFrame.size.width, ScreenFrame.size.height-(GetToTopDistance+15+30)) style:UITableViewStylePlain];
    maintable.dataSource = self;
    maintable.delegate = self;
    [self.view addSubview:maintable];

}
-(void)segControlAction{
    if (segControl.selectedSegmentIndex == 0) {
        curIndex = 0;
        [self reCreatMainTable];
    }
    if (segControl.selectedSegmentIndex == 1) {
        curIndex = 1;
        [self reCreatMainTable];

    }
    if (segControl.selectedSegmentIndex == 2) {
        curIndex = 2;
        [self reCreatMainTable];

    }
    if (segControl.selectedSegmentIndex == 3) {
        curIndex = 3;
        [self reCreatMainTable];
     }
}
- (void)getStationMapInfoSelectByArea{
    
    if (!dataArr) {
        dataArr = [NSMutableArray array];
    }
    [dataArr removeAllObjects];
    [maintable reloadData];
    
    [AppHelper showHUD:@"正在获取站点数据" inView:self.view];
    NSMutableArray *arr=[NSMutableArray array];
    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%d",[areaArr[0] intValue]],@"areaID", nil]];
    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"20",@"pageSize", nil]];
    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%d",page],@"pageIndex", nil]];
    
    NSString *soapMsg=[SoapHelper arrayToDefaultSoapMessage:arr methodName:@"StationMapInfoSelectByArea"];
    [helper asyncServiceMethod:@"StationMapInfoSelectByArea" soapMessage:soapMsg];
}
-(void)requestFinishedMessageForCount:(NSString *)count{
    NSInteger totoal = [count intValue]/4;
    if ([count intValue]%4!=0) {
        totoal++;
    }
    totoalPage = totoal;
    totoalPagelb.text = [NSString stringWithFormat:@"共 %d 页",totoal];
}
#pragma mark -
#pragma mark WebServices delegate Methods
-(void)requestFinishedMessage:(NSString*)xml{
    [AppHelper removeHUD];
    NSData*data = [Base64 decodeBase64WithString:xml];
    NSError *error = nil;
    NSData*tempData = [ASIDataDecompressor uncompressData:data error:&error];
    
    NSStringEncoding gbkEncoding = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
    {
        NSString *xmlstr = [[NSString alloc] initWithData:tempData encoding:gbkEncoding];
        NSLog(@"返回的xml数据:%@",xmlstr);
        NSString*preSrt = [xmlstr componentsSeparatedByString:@"@"][0];
        NSLog(@"preSrt:%@",preSrt);
        NSString*sufSrt = [xmlstr substringFromIndex:preSrt.length+1];
        
        if (!dataArr) {
            dataArr = [NSMutableArray array];
        }
        [dataArr removeAllObjects];
        
        dataArr = [[sufSrt JSONValue] mutableCopy];
        
        if (!didMarkArr) {
            didMarkArr = [NSMutableArray array];
        }
        [didMarkArr removeAllObjects];
        
        if (!notMarkCountArr) {
            notMarkCountArr = [NSMutableArray array];
        }
        [notMarkCountArr removeAllObjects];
         for (NSArray*ar in dataArr) {
                  if (![ar[1] isEqual:[NSNull null]] && ![ar[1] isEqualToString:@""]) {
                     [didMarkArr addObject:ar];
                 }else{
                     [notMarkCountArr addObject:ar];
               }
         }
        [maintable reloadData];
     }
    
    if (error) {
        NSLog(@"%@",[error description]);
    }
}
-(void)requestFailedMessage:(NSError*)error{
    [AppHelper removeHUD];
    NSLog(@"请求失败:%@\n",[error description]);
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (curIndex == 0) {
        return dataArr.count;
    }
    if (curIndex == 1) {
        return didMarkArr.count;
    }
    if (curIndex == 2) {
        return notMarkCountArr.count;
    }
    if (curIndex == 3) {
        return didMarkArr.count;
    }
    return dataArr.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString*cellid = [NSString stringWithFormat:@"%d",indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    if (!cell) {
        cell= [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellid];
        {
            UILabel*lb = [[UILabel alloc] initWithFrame:CGRectMake(15, 5, 140, 30)];
            lb.font = [UIFont systemFontOfSize:14];
            lb.backgroundColor = [UIColor clearColor];
            lb.textColor = [UIColor blackColor];
            lb.textAlignment = NSTextAlignmentCenter;
            [cell.contentView addSubview:lb];
            lb.tag = 10;
        }
        {
            UILabel*lb = [[UILabel alloc] initWithFrame:CGRectMake(160, 5, 160, 30)];
            lb.font = [UIFont systemFontOfSize:14];
            lb.backgroundColor = [UIColor clearColor];
            lb.textColor = [UIColor blackColor];
            lb.textAlignment = NSTextAlignmentCenter;
            lb.adjustsFontSizeToFitWidth = YES;
            [cell.contentView addSubview:lb];
            lb.tag = 11;
        }
    }
    if (curIndex == 0) {
        if (!dataArr || dataArr.count == 0) {
            return cell;
        }
        UILabel *lb = (UILabel *)[cell.contentView viewWithTag:10];
        UILabel *lb1 = (UILabel *)[cell.contentView viewWithTag:11];
        lb.text = dataArr[indexPath.row][0];
        if (![dataArr[indexPath.row][1] isEqual:[NSNull null]] && ![dataArr[indexPath.row][1] isEqualToString:@""]) {
            lb1.text = @"已标记";
        }else{
            lb1.text = @"未标记";
        }

    }
    if (curIndex == 1) {
        if (!didMarkArr || didMarkArr.count == 0) {
            return cell;
        }
        UILabel *lb = (UILabel *)[cell.contentView viewWithTag:10];
        UILabel *lb1 = (UILabel *)[cell.contentView viewWithTag:11];
        lb.text = didMarkArr[indexPath.row][0];
        if (![didMarkArr[indexPath.row][1] isEqual:[NSNull null]] && ![didMarkArr[indexPath.row][1] isEqualToString:@""]) {
            lb1.text = @"已标记";
        }else{
            lb1.text = @"未标记";
        }
    }
    if (curIndex == 2) {
        if (!notMarkCountArr || notMarkCountArr.count == 0) {
            return cell;
        }
        UILabel *lb = (UILabel *)[cell.contentView viewWithTag:10];
        UILabel *lb1 = (UILabel *)[cell.contentView viewWithTag:11];
        lb.text = notMarkCountArr[indexPath.row][0];
        if (![notMarkCountArr[indexPath.row][1] isEqual:[NSNull null]] && ![notMarkCountArr[indexPath.row][1] isEqualToString:@""]) {
            lb1.text = @"已标记";
        }else{
            lb1.text = @"未标记";
        }
     }
    if (curIndex == 3) {
        if (!didMarkArr || didMarkArr.count == 0) {
            return cell;
        }
        UILabel *lb = (UILabel *)[cell.contentView viewWithTag:10];
        UILabel *lb1 = (UILabel *)[cell.contentView viewWithTag:11];
        lb.text = didMarkArr[indexPath.row][0];
        if (![didMarkArr[indexPath.row][1] isEqual:[NSNull null]] && ![didMarkArr[indexPath.row][1] isEqualToString:@""]) {
            lb1.text = @"查看照片";
        }else{
            
         }
    }

       return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
   
    if (curIndex == 0) {
        if (![dataArr[indexPath.row][1] isEqual:[NSNull null]] && ![dataArr[indexPath.row][1] isEqualToString:@""]) {
            StationMapDetailViewController*ctrl = [[StationMapDetailViewController alloc] initWithNibName:@"StationMapDetailViewController" bundle:nil];
            ctrl.staionArr = dataArr[indexPath.row];
            [self.navigationController pushViewController:ctrl animated:YES];
        }else{
            StationMapDetail2ViewController*ctrl = [[StationMapDetail2ViewController alloc] initWithNibName:@"StationMapDetail2ViewController" bundle:nil];
            ctrl.staionArr = dataArr[indexPath.row];
            [self.navigationController pushViewController:ctrl animated:YES];
        }
    }
    if (curIndex == 1) {
        StationMapDetailViewController*ctrl = [[StationMapDetailViewController alloc] initWithNibName:@"StationMapDetailViewController" bundle:nil];
         ctrl.staionArr = didMarkArr[indexPath.row];
        [self.navigationController pushViewController:ctrl animated:YES];
     }
    if (curIndex == 2) {
        StationMapDetail2ViewController*ctrl = [[StationMapDetail2ViewController alloc] initWithNibName:@"StationMapDetail2ViewController" bundle:nil];
        ctrl.staionArr = notMarkCountArr[indexPath.row];
        [self.navigationController pushViewController:ctrl animated:YES];
    }
    if (curIndex == 3) {
        StationMapDetailViewController*ctrl = [[StationMapDetailViewController alloc] initWithNibName:@"StationMapDetailViewController" bundle:nil];
        ctrl.staionArr = didMarkArr[indexPath.row];
        [self.navigationController pushViewController:ctrl animated:YES];
    }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
