//
//  MapDetailViewController.h
//  DongYuan
//
//  Created by 鄢罗军 on 14/10/13.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import "BASEUIViewController.h"
#import <MapKit/MapKit.h>

@interface MapDetailViewController : BASEUIViewController<MKMapViewDelegate,CLLocationManagerDelegate>
{
    MKMapView *mapView;
    CLLocationManager *loactionManager;
    
    NSMutableArray*mapDatas;
}
-(void)setAnnotationsWithCoordnations:(NSMutableArray *)coords;
@end
