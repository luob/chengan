//
//  HomeViewController.h
//  DongYuan
//
//  Created by 鄢罗军 on 14/9/19.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StationCheckViewController.h"
#import "NormalHuanReStationViewController.h"
#import "WeatherViewController.h"
#import "ZNHuanReStationViewController.h"
#import "CheckPointViewController.h"
#import "StationMapViewController.h"

#import "SelecteAreaViewController.h"

#import "WebServices.h"
#import "SelecteCheckPointViewController.h"
#import "SelecteStationAndCheckPointViewController.h"
#import "CheckPointCheckViewController.h"
#import "MJViewController.h"

@interface HomeViewController : UIViewController<WebServiceDelegate,SelecteAreaDelegate,SelecteCheckPointDelegate,SelecteStationAndCheckPointDelegate>{
    WebServices *helper;

    SelecteAreaViewController*selectAraeCtrl,*mapStationCtrl;
    SelecteCheckPointViewController*selectCheckPointCtrl;
    SelecteStationAndCheckPointViewController*selectStationAndCheckPointCtrl;
    MJViewController*MJViewCtrl;
    IBOutlet UIScrollView*scroll;

}

-(IBAction)btnAction:(id)sender;
@end
