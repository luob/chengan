//
//  CheckPointViewController.h
//  DongYuan
//
//  Created by 鄢罗军 on 14/10/9.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import "BASEUIViewController.h"
#import "WebServices.h"

@interface CheckPointViewController : BASEUIViewController<WebServiceDelegate,UITableViewDataSource,UITableViewDelegate>
{
    WebServices *helper;
    
     NSMutableArray*dataArr;
    UITableView*maintable;
    int page;
    
    UILabel*totoalPagelb;
    
    UITextField*curPageField;
    
    UIView*inputPageView;
    UITextField*inputPageField;
    
    int totoalPage;
    
    NSString*checkPointName;
    
    UILabel*titlelb;

}
-(void)setCheckPointName:(NSString *)name;
@end
