//
//  HistoryNormalStationResultViewController.h
//  DongYuan
//
//  Created by 鄢罗军 on 14/10/13.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import "BASEUIViewController.h"
#import "WebServices.h"
@interface HistoryCheckpointResultViewController : BASEUIViewController<WebServiceDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>{
    WebServices *helper;
    IBOutlet UILabel*toplb;
    NSMutableArray*dataArr;
    UITableView*maintable;
    NSString *curCheckPointName,*curendTime,*curStartTime;
    BOOL curhour;
    
    int page;
    
    UILabel*totoalPagelb;
    
    UITextField*curPageField;
    
    UIView*inputPageView;
    UITextField*inputPageField;
    
    int totoalPage;
    
}

-(void)requestWithCheckPointName:(NSString *)checkPointName startTime:(NSString *)startTime endTime:(NSString *)endTime housSwitch:(BOOL)hour;
@end
