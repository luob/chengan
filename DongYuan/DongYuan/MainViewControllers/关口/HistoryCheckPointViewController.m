//
//  HistoryNormalStationViewController.m
//  DongYuan
//
//  Created by 鄢罗军 on 14/10/13.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import "HistoryCheckPointViewController.h"
#import "SoapHelper.h"
#import "ASIDataDecompressor.h"
#import "Base64.h"
#import "JSON.h"

#import "HistoryCheckpointResultViewController.h"
@interface HistoryCheckPointViewController ()

@end

@implementation HistoryCheckPointViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)backAction{
    [self dismissViewControllerAnimated:NO completion:nil];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self AddCustomNavigationBar];
    [self AddCustomLeftImageItem:[UIImage imageNamed:@"返回"] WithSelector:@selector(backAction) Target:self];
    [self AddCustomNavigationbarTitle:@"选择关口"];
    
     [self reCreatTableView];
     
}
-(IBAction)ConfirmAction{
     if (!curCheckPointName || [curCheckPointName isEqualToString:@""]) {
        [UIAlertView ShowAlertViewWithMsg:@"请输入关口名称"];
        return;
    }
     if (!startTime || [startTime isEqualToString:@""]) {
        [UIAlertView ShowAlertViewWithMsg:@"请输入开始时间"];
        return;
    }
    if (!endTime || [endTime isEqualToString:@""]) {
        [UIAlertView ShowAlertViewWithMsg:@"请输入结束时间"];
        return;
    }
    HistoryCheckpointResultViewController *ctrl = [[HistoryCheckpointResultViewController alloc] initWithNibName:@"HistoryCheckpointResultViewController" bundle:nil];
    [ctrl requestWithCheckPointName:curCheckPointName startTime:startTime endTime:endTime housSwitch:hourswitch];
    [self presentViewController:ctrl animated:NO completion:nil];
}
-(void)reCreatTableView{
    if (mainTable) {
        [mainTable removeFromSuperview];
        mainTable = nil;
    }
    mainTable = [[UITableView alloc] initWithFrame:CGRectMake(0, GetToTopDistance, self.view.frame.size.width, 44*4) style:UITableViewStylePlain];
    mainTable.dataSource = self;
    mainTable.delegate = self;
    [self.view addSubview:mainTable];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString*cellid = @"cellid";
    UITableViewCell*cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellid];
        
            UILabel*lb = [[UILabel alloc] initWithFrame:CGRectMake(140, 5, 140, 34)];
            lb.backgroundColor = [UIColor clearColor];
            lb.font = [UIFont systemFontOfSize:14];
            [cell.contentView addSubview:lb];
            lb.tag = 10;
            lb.textAlignment = NSTextAlignmentCenter;
            
            if (indexPath.row == 3) {
                cell.textLabel.text = @"按小时计算";
                hourSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 50, 40)];
                cell.accessoryView = hourSwitch;
                [hourSwitch addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
            }
    }

        UILabel *lb = (UILabel *)[cell.contentView viewWithTag:10];
    if (indexPath.row == 0) {
        cell.textLabel.text = @"关口";
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        if (curCheckPointName && ![curCheckPointName isEqualToString:@""]) {
            lb.text = curCheckPointName;
        }
    }
    
    if (indexPath.row == 1) {
        cell.textLabel.text = @"起始时间";
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        if (startTime && ![startTime isEqualToString:@""]) {
            lb.text = startTime;
        }
    }
    if (indexPath.row == 2) {
        cell.textLabel.text = @"结束时间";
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        if (endTime && ![endTime isEqualToString:@""]) {
            lb.text = endTime;
        }
    }
    if (indexPath.row == 5) {
        cell.textLabel.text = @"按小时计算";
        [hourSwitch setOn:hourswitch];
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 0) {
        selectCheckPointCtrl = [[SelecteCheckPointViewController alloc] initWithNibName:@"SelecteCheckPointViewController" bundle:nil];
        [self.view addSubview:selectCheckPointCtrl.view];
        NSMutableArray*arr = [NSMutableArray array];
        [arr addObject:@"一级中继泵站"];
        [arr addObject:@"二级中继泵站"];
        [arr addObject:@"陈塘电厂"];
        [arr addObject:@"陈塘新厂"];

        [selectCheckPointCtrl setStationsArrs:arr];
        selectCheckPointCtrl.delegate = self;
    }
    
    if (indexPath.row == 1) {
        datepicker = [[MyDatePicker alloc] initWithFrame:CGRectMake(0, ScreenFrame.size.height, ScreenFrame.size.width, ScreenFrame.size.height)];
        datepicker.delegate = self;
        [[[UIApplication sharedApplication] keyWindow] addSubview:datepicker];
        [UIView animateWithDuration:0.3 animations:^{
            datepicker.frame = CGRectMake(0, 0, ScreenFrame.size.width, ScreenFrame.size.height);
        }];
        curDateIndex = 0;
    }
    if (indexPath.row == 2) {
        datepicker = [[MyDatePicker alloc] initWithFrame:CGRectMake(0, ScreenFrame.size.height, ScreenFrame.size.width, ScreenFrame.size.height)];
        datepicker.delegate = self;
        [[[UIApplication sharedApplication] keyWindow] addSubview:datepicker];
        [UIView animateWithDuration:0.3 animations:^{
            datepicker.frame = CGRectMake(0, 0, ScreenFrame.size.width, ScreenFrame.size.height);
        }];
        curDateIndex = 1;
    }
}
#pragma mark 时间控件 回调
-(void)MyDatePickerDidSelectedDate:(NSString *)dateString{
    if (!dateString) { //取消按钮
        [UIView animateWithDuration:0.1 animations:^{
            datepicker.frame = CGRectMake(0, ScreenFrame.size.height, ScreenFrame.size.width, ScreenFrame.size.height);
        }completion:^(BOOL finished){
            [datepicker removeFromSuperview];
            datepicker = nil;
        }];
        return;
    }
    if (dateString) {
        if (curDateIndex == 0) {
            startTime = dateString;
        }
        if (curDateIndex == 1) {
            endTime = dateString;
        }
        [mainTable reloadData];
        if (datepicker) {
            [UIView animateWithDuration:0.1 animations:^{
                
                datepicker.frame = CGRectMake(0, ScreenFrame.size.height, ScreenFrame.size.width, ScreenFrame.size.height);
                
            }completion:^(BOOL finished){
                
                [datepicker removeFromSuperview];
                datepicker = nil;
            }];
        }
    }
}

-(void)switchAction:(UISwitch *)sw{
    if (sw == hourSwitch) {
        hourswitch = sw.isOn;
    }
}

-(void)SelecteCheckPointConfirmBtnClicked:(NSString *)selectedStation :(SelecteCheckPointViewController*)selectAreaCtrl{
    curCheckPointName = selectedStation;
    [mainTable reloadData];
}


-(BOOL)shouldAutorotate{
    return YES;
}
-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
