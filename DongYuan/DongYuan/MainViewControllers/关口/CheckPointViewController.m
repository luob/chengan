//
//  CheckPointViewController.m
//  DongYuan
//
//  Created by 鄢罗军 on 14/10/9.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import "CheckPointViewController.h"
#import "SoapHelper.h"
#import "ASIDataDecompressor.h"
#import "Base64.h"
#import "JSON.h"

#import "HistoryCheckPointViewController.h"

@interface CheckPointViewController ()

@end

@implementation CheckPointViewController
-(void)setCheckPointName:(NSString *)name{
    checkPointName = name;
}
- (void)getJAMathDataGWC_Select{
    
    [AppHelper showHUD:@"正在获取关口数据" inView:self.view];
    NSMutableArray *arr=[NSMutableArray array];
    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:checkPointName?checkPointName:@"",@"gWCName", nil]];
//    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"2014-09-01 08:11:00",@"startTime", nil]];
//    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"2014-10-09 09:11:00",@"endTime", nil]];
    
    NSString *soapMsg=[SoapHelper arrayToDefaultSoapMessage:arr methodName:@"CAHistoryDataGWC_SelectNow"];
    [helper asyncServiceMethod:@"CAHistoryDataGWC_SelectNow" soapMessage:soapMsg];
}

#pragma mark -
#pragma mark WebServices delegate Methods
-(void)requestFinishedMessage:(NSString*)xml{
    [AppHelper removeHUD];
    NSData*data = [Base64 decodeBase64WithString:xml];
    NSError *error = nil;
    NSData*tempData = [ASIDataDecompressor uncompressData:data error:&error];
    
    NSStringEncoding gbkEncoding = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
    {
        NSString *xmlstr = [[NSString alloc] initWithData:tempData encoding:gbkEncoding];
        NSLog(@"返回的xml数据:%@",xmlstr);
        if ([xmlstr isEqualToString:@""]) {
            [UIAlertView ShowAlertViewWithMsg:@"数据为空"];
            return;
        }
        NSString*preSrt = [xmlstr componentsSeparatedByString:@"@"][0];
        NSLog(@"preSrt:%@",preSrt);
        NSString*sufSrt = [xmlstr componentsSeparatedByString:@"@"][1];
        NSArray *result = [sufSrt JSONValue];
        if ([result count] <= 0) {
            [UIAlertView ShowAlertViewWithMsg:@"数据为空"];
            return;
        }
        if (!dataArr) {
            dataArr = [NSMutableArray array];
        }
        dataArr = [result mutableCopy][0];
        [maintable reloadData];
    }
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)backAction{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)historyBtnAction{
    HistoryCheckPointViewController*seartchCtrl = [[HistoryCheckPointViewController alloc] initWithNibName:@"HistoryCheckPointViewController" bundle:nil];
    [self presentViewController:seartchCtrl animated:NO completion:nil];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self AddCustomNavigationBar];
    [self AddCustomLeftImageItem:[UIImage imageNamed:@"返回"] WithSelector:@selector(backAction) Target:self];
   titlelb = [self AddCustomNavigationbarTitle:@"关口数据"];
    titlelb.text = checkPointName;

    helper=[[WebServices alloc] initWithDelegate:self];

    [self getJAMathDataGWC_Select];
    
    maintable = [[UITableView alloc] initWithFrame:CGRectMake(0, GetToTopDistance+10, ScreenFrame.size.width, ScreenFrame.size.height-(GetToTopDistance+10)) style:UITableViewStylePlain];
    
    maintable.dataSource = self;
    maintable.delegate = self;
    [self.view addSubview:maintable];
    
    UIButton *historyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    historyBtn.frame = CGRectMake(self.view.frame.size.width-95, GetTopHeight, 90, 44);
    [historyBtn addTarget:self action:@selector(historyBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [historyBtn setTitle:@"历史数据" forState:UIControlStateNormal];
    [historyBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:historyBtn];
 
    // Do any additional setup after loading the view from its nib.
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArr.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cellid = @"cellid";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: cellid];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellid];
        {
            UILabel*lb = [[UILabel alloc] initWithFrame:CGRectMake(15, 5, 140, 30)];
            lb.font = [UIFont systemFontOfSize:14];
            lb.backgroundColor = [UIColor clearColor];
            lb.textColor = [UIColor blackColor];
            lb.textAlignment = NSTextAlignmentCenter;
            [cell.contentView addSubview:lb];
            lb.tag = 10;
        }
        {
            UILabel*lb = [[UILabel alloc] initWithFrame:CGRectMake(160, 5, 160, 30)];
            lb.font = [UIFont systemFontOfSize:14];
            lb.backgroundColor = [UIColor clearColor];
            lb.textColor = [UIColor blackColor];
            lb.textAlignment = NSTextAlignmentCenter;
            lb.adjustsFontSizeToFitWidth = YES;
            [cell.contentView addSubview:lb];
            lb.tag = 11;
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    UILabel *lb = (UILabel *)[cell.contentView viewWithTag:10];
    UILabel *lb1 = (UILabel *)[cell.contentView viewWithTag:11];

    if (!dataArr || dataArr.count == 0) {
        return cell;
    }
    if (indexPath.row == 0) {
        lb.text = @"关口名称";
        lb1.text = dataArr[1];
     }
    if (indexPath.row == 1) {
        lb.text = @"时间";
        lb1.text = dataArr[0];
    }
    if (indexPath.row == 2) {
        lb.text = @"供水温度(℃)";
        lb1.text = [NSString stringWithFormat:@"%.02f",[dataArr[2] floatValue]];
    }
    if (indexPath.row == 3) {
        lb.text = @"回水温度(℃)";
        lb1.text = [NSString stringWithFormat:@"%.02f",[dataArr[3] floatValue]];
     }
    if (indexPath.row == 4) {
        lb.text = @"回水压力(MPa)";
        lb1.text = [NSString stringWithFormat:@"%.02f",[dataArr[4] floatValue]];

    }
    if (indexPath.row == 5) {
        lb.text = @"供水压力(MPa)";
        lb1.text = [NSString stringWithFormat:@"%.02f",[dataArr[5] floatValue]];

    }
    if (indexPath.row == 6) {
        lb.text = @"供水流量(t/h)";
        lb1.text = [NSString stringWithFormat:@"%.02f",[dataArr[6] floatValue]];

    }
    if (indexPath.row == 7) {
        lb.text = @"回水流量(t/h)";
        lb1.text = [NSString stringWithFormat:@"%.02f",[dataArr[7] floatValue]];

    }
 
    return cell;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end






