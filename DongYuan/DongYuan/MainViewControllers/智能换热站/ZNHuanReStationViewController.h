//
//  ZNHuanReStationViewController.h
//  DongYuan
//
//  Created by 鄢罗军 on 14/10/9.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import "BASEUIViewController.h"
#import "SelecteZNStationViewController.h"
#import "WebServices.h"
@interface ZNHuanReStationViewController : BASEUIViewController<UITableViewDataSource,UITableViewDelegate,WebServiceDelegate,UITextFieldDelegate,SelecteZNStationDelegate>
{
    WebServices *helper;
    
    IBOutlet UILabel*toplb;
    NSMutableArray*dataArr;
    UITableView*maintable;
    int page;
    
    UILabel*totoalPagelb;
    
    UITextField*curPageField;
    
    UIView*inputPageView;
    UITextField*inputPageField;
    
    SelecteZNStationViewController*selectZNStation;
    
    int totoalPage;
    
    BOOL isGetZNStation;
    
    BOOL isGetSerachData;
    
    NSString *curStationName;
}
 
@end
