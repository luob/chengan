//
//  SearchHuanReStationViewController.h
//  DongYuan
//
//  Created by 鄢罗军 on 14/9/30.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BASEUIViewController.h"
#import "WebServices.h"

#import "SelecteAreaViewController.h"
#import "SelecteStationViewController.h"

@interface SearchHuanReStationViewController : BASEUIViewController<WebServiceDelegate,UITableViewDataSource,UITableViewDelegate,SelecteAreaDelegate,SelecteStationDelegate>{
    WebServices *helper;

    NSMutableArray*stationNames;
    
    UITableView*mainTable;
    
    UILabel*arealb,*stationlb;
    
    SelecteAreaViewController*selectAraeCtrl;
    SelecteStationViewController*selectStationCtrl;
    
    NSMutableArray *curAreaArr;
    NSString*curStationName;
}

@end
