//
//  HistoryNormalStationResultViewController.h
//  DongYuan
//
//  Created by 鄢罗军 on 14/10/13.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import "BASEUIViewController.h"
#import "WebServices.h"
@interface HistoryNormalStationResultViewController : BASEUIViewController<WebServiceDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>{
    WebServices *helper;
      IBOutlet UILabel*toplb;
    NSMutableArray*dataArr;
    UITableView*maintable;
    NSString *curstationName,*curdevid,*curstartTime,*curendTime,*curArea;
    BOOL curhour;
    
    int page;
    
    UILabel*totoalPagelb;
    
    UITextField*curPageField;
    
    UIView*inputPageView;
    UITextField*inputPageField;
    
    int totoalPage;

}

-(void)requestWithStationName:(NSString *)stationName deviceid:(NSString *)devid curArea:(NSString*)Area startTime:(NSString *)startTime endTime:(NSString *)endTime housSwitch:(BOOL)hour;
@end
