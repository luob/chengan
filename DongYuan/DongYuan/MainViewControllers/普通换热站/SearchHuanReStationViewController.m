//
//  SearchHuanReStationViewController.m
//  DongYuan
//
//  Created by 鄢罗军 on 14/9/30.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import "SearchHuanReStationViewController.h"
#import "SoapHelper.h"
#import "ASIDataDecompressor.h"
#import "Base64.h"
#import "JSON.h"

#import "SearchNormalStationResultViewController.h"
@interface SearchHuanReStationViewController ()

@end

@implementation SearchHuanReStationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)backAction{
    [self dismissViewControllerAnimated:NO completion:nil];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
 

    UILabel *titlelb = [[UILabel alloc] initWithFrame:CGRectMake(50, GetTopHeight, 220, 44)];
    titlelb.textColor = [UIColor blackColor];
    titlelb.text = @"普通热站查询";
    titlelb.textAlignment = NSTextAlignmentCenter;
    titlelb.backgroundColor = [UIColor clearColor];
    titlelb.font = [UIFont systemFontOfSize:kTitleFont];
    [self.view addSubview:titlelb];
    titlelb.userInteractionEnabled = NO;
    
    NSString *title = @"取消";
    
    UIButton *BackBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    CGSize size = [title sizeWithFont:[UIFont systemFontOfSize:18] constrainedToSize:CGSizeMake(MAXFLOAT, 44)];
    if (size.width < 50) {
        size.width = 50;
    }
    BackBtn.frame = CGRectMake(self.view.frame.size.width-size.width-10, GetTopHeight, size.width, 44);
    [BackBtn addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [BackBtn setTitle:title forState:UIControlStateNormal];
    BackBtn.titleLabel.font = [UIFont systemFontOfSize:18];
    [BackBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.view addSubview:BackBtn];
    
    self.view.backgroundColor = [UIColor ColorWithHexString:@"f0eff4"];
    
    // Do any additional setup after loading the view from its nib.
    
    helper=[[WebServices alloc] initWithDelegate:self];
    
    mainTable = [[UITableView alloc] initWithFrame:CGRectMake(0, GetToTopDistance+10, self.view.frame.size.width, 100) style:UITableViewStylePlain];
    mainTable.dataSource = self;
    mainTable.delegate = self;
    [self.view addSubview:mainTable];
    mainTable.scrollEnabled = NO;
    
    UIButton*btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setBackgroundColor:[UIColor ColorWithHexString:@"105694"]];
    [btn setFrame:CGRectMake(0, GetToTopDistance+110, self.view.frame.size.width-10, 40)];
    [btn setCenter:CGPointMake(self.view.frame.size.width/2, GetToTopDistance+110+40)];
    [btn setTitle:@"确  定" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:20];
    [btn addTarget:self action:@selector(confirmBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
    
    curAreaArr = [[SharedDelegate.areaArrs mutableCopy] objectAtIndex:0];
}
 
-(void)confirmBtnAction{
    //根据热站名称查询 热站数据
    //SimpleNowDataAllByParentStationName
     SearchNormalStationResultViewController *ctrl = [[SearchNormalStationResultViewController alloc] initWithNibName:@"SearchNormalStationResultViewController" bundle:nil];
    ctrl.stationName = curStationName;
    [self presentViewController:ctrl animated:NO completion:nil];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cellid = @"cellid";
    UITableViewCell*cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellid];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        if (indexPath.row == 0) {
            {
                UILabel*lb = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 40, 30)];
                lb.text = @"区域";
                lb.font = [UIFont systemFontOfSize:16];
                lb.backgroundColor = [UIColor clearColor];
                lb.textColor = [UIColor blackColor];
                [cell.contentView addSubview:lb];
            }
            {
                arealb = [[UILabel alloc] initWithFrame:CGRectMake(70, 10, 170, 30)];
                arealb.font = [UIFont systemFontOfSize:16];
                arealb.backgroundColor = [UIColor clearColor];
                arealb.textColor = [UIColor blackColor];
                [cell.contentView addSubview:arealb];
            }
        }
        if (indexPath.row == 1) {
            {
                UILabel*lb = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 40, 30)];
                lb.text = @"站点";
                lb.font = [UIFont systemFontOfSize:16];
                lb.backgroundColor = [UIColor clearColor];
                lb.textColor = [UIColor blackColor];
                [cell.contentView addSubview:lb];
            }
            {
                stationlb = [[UILabel alloc] initWithFrame:CGRectMake(70, 10, 170, 30)];
                stationlb.font = [UIFont systemFontOfSize:16];
                stationlb.backgroundColor = [UIColor clearColor];
                stationlb.textColor = [UIColor blackColor];
                [cell.contentView addSubview:stationlb];
            }

        }
     }
    if (indexPath.row == 0) {
        arealb.text = curAreaArr[1];
    }
    if (indexPath.row == 1) {
        stationlb.text = curStationName;
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0) {
        selectAraeCtrl = [[SelecteAreaViewController alloc] initWithNibName:@"SelecteAreaViewController" bundle:nil];
        [self.view addSubview:selectAraeCtrl.view];
        [selectAraeCtrl setAreaArrs:SharedDelegate.areaArrs];
        selectAraeCtrl.delegate = self;
     }
    if (indexPath.row == 1) {
        [self getStationByAreaID:curAreaArr[0]];
    }
 }
-(void)SelecteAreaConfirmBtnClicked:(NSArray *)selectedArr :(SelecteAreaViewController *)selectAreaCtrl{
    curAreaArr = [selectedArr mutableCopy];
    
    [mainTable reloadData];
}
- (void)getStationByAreaID:(NSString *)areaID{
    
//    [AppHelper showHUD:@"正在获取换热站数据" inView:self.view];
    NSMutableArray *arr=[NSMutableArray array];
    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%d",[areaID intValue]],@"areaID", nil]];
    
    NSString *soapMsg=[SoapHelper arrayToDefaultSoapMessage:arr methodName:@"StationByAreaID"];
    [helper asyncServiceMethod:@"StationByAreaID" soapMessage:soapMsg];
}

#pragma mark -
#pragma mark WebServices delegate Methods
-(void)requestFinishedMessage:(NSString*)xml{

    NSData*data = [Base64 decodeBase64WithString:xml];
    NSError *error = nil;
    NSData*tempData = [ASIDataDecompressor uncompressData:data error:&error];
    
    NSStringEncoding gbkEncoding = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
    {
        NSString *xmlstr = [[NSString alloc] initWithData:tempData encoding:gbkEncoding];
        NSLog(@"返回的xml数据:%@",xmlstr);
        NSString*preSrt = [xmlstr componentsSeparatedByString:@"@"][0];
        NSLog(@"preSrt:%@",preSrt);
        NSString*sufSrt = [xmlstr componentsSeparatedByString:@"@"][1];
        
        if (!stationNames) {
            stationNames = [NSMutableArray array];
         }
        [stationNames removeAllObjects];
        
        for (NSArray*ar in [sufSrt JSONValue]) {
            [stationNames addObject:ar[0]];
        }
         selectStationCtrl= [[SelecteStationViewController alloc] initWithNibName:@"SelecteStationViewController" bundle:nil];
        [self.view addSubview:selectStationCtrl.view];
        [selectStationCtrl setStationsArrs:stationNames];
        selectStationCtrl.delegate = self;
    }
    
    if (error) {
        NSLog(@"%@",[error description]);
    }
}
-(void)SelecteStationConfirmBtnClicked:(NSString *)selectedStation :(SelecteStationViewController *)selectAreaCtrl{
    curStationName = selectedStation;
    [mainTable reloadData];
}
-(void)requestFailedMessage:(NSError*)error{

    NSLog(@"请求失败:%@\n",[error description]);
}
//
-(BOOL)shouldAutorotate{
    return YES;
}
-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;//这里写你需要的方向。看好了，别写错了
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
