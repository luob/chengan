//
//  HistoryNormalStationResultViewController.m
//  DongYuan
//
//  Created by 鄢罗军 on 14/10/13.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import "HistoryNormalStationResultViewController.h"
#import "SoapHelper.h"
#import "ASIDataDecompressor.h"
#import "Base64.h"
#import "JSON.h"

#define kTotoalWidth 540

@interface HistoryNormalStationResultViewController ()

@end

@implementation HistoryNormalStationResultViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)backAction{
    [self dismissViewControllerAnimated:NO completion:nil];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44+GetTopHeight)];
    image.image = [[UIImage imageNamed:@"home_01.png"] stretchableImageWithLeftCapWidth:2 topCapHeight:10];
    [self.view addSubview:image];
    
    UILabel*titlelb = [[UILabel alloc] initWithFrame:CGRectMake(0, GetTopHeight, self.view.frame.size.width, 44)];
    titlelb.text = @"历史数据";
    titlelb.textAlignment = NSTextAlignmentCenter;
    titlelb.textColor = [UIColor whiteColor];
    titlelb.font = [UIFont systemFontOfSize:18];
    [self.view addSubview:titlelb];
    
    UIButton *BackBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    BackBtn.frame = CGRectMake(0, GetTopHeight, 50, 44);
    [BackBtn addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [BackBtn setImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
    [self.view addSubview:BackBtn];
    
    float width = kTotoalWidth;
    
    UIScrollView*mainScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 44+GetTopHeight, self.view.frame.size.width, self.view.frame.size.height-(44+GetTopHeight+50))];
    [self.view addSubview:mainScroll];
    [mainScroll setContentSize:CGSizeMake(self.view.frame.size.width+width, self.view.frame.size.height-(44+GetTopHeight+50))];
    
    maintable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width+width, self.view.frame.size.height-(44+GetTopHeight+50)) style:UITableViewStylePlain];
    maintable.dataSource = self;
    maintable.delegate = self;
    [mainScroll addSubview:maintable];
    
    UIView*botContentView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-50, self.view.frame.size.width, 50)];
    [self.view addSubview:botContentView];
    botContentView.backgroundColor = [UIColor ColorWithHexString:@"a9a4aa"];
    
    totoalPagelb = [[UILabel alloc] initWithFrame:CGRectMake(115, 10, 80, 30)];
    totoalPagelb.backgroundColor = [UIColor clearColor];
    totoalPagelb.font = [UIFont systemFontOfSize:14];
    totoalPagelb.textColor = [UIColor blackColor];
    [botContentView addSubview:totoalPagelb];
    
    UIButton*leftBnt = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBnt.frame = CGRectMake(200, 10, 40, 30);
    [leftBnt setTitle:@"|<" forState:UIControlStateNormal];
    [leftBnt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    leftBnt.titleLabel.font = [UIFont boldSystemFontOfSize:22];
    leftBnt.backgroundColor = [UIColor whiteColor];
    [botContentView addSubview:leftBnt];
    [leftBnt addTarget:self action:@selector(leftBntAction) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton*left1Bnt = [UIButton buttonWithType:UIButtonTypeCustom];
    left1Bnt.frame = CGRectMake(245, 10, 40, 30);
    [left1Bnt setTitle:@"<" forState:UIControlStateNormal];
    [left1Bnt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    left1Bnt.titleLabel.font = [UIFont boldSystemFontOfSize:26];
    left1Bnt.backgroundColor = [UIColor whiteColor];
    [botContentView addSubview:left1Bnt];
    [left1Bnt addTarget:self action:@selector(left1BntAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton*rightBnt = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBnt.frame = CGRectMake(290, 10, 40, 30);
    [rightBnt setTitle:@">" forState:UIControlStateNormal];
    [rightBnt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    rightBnt.titleLabel.font = [UIFont boldSystemFontOfSize:26];
    rightBnt.backgroundColor = [UIColor whiteColor];
    [botContentView addSubview:rightBnt];
    [rightBnt addTarget:self action:@selector(rightBntAction) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton*right1Bnt = [UIButton buttonWithType:UIButtonTypeCustom];
    right1Bnt.frame = CGRectMake(335, 10, 40, 30);
    [right1Bnt setTitle:@">|" forState:UIControlStateNormal];
    [right1Bnt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    right1Bnt.titleLabel.font = [UIFont boldSystemFontOfSize:22];
    right1Bnt.backgroundColor = [UIColor whiteColor];
    [botContentView addSubview:right1Bnt];
    [right1Bnt addTarget:self action:@selector(right1BntAction) forControlEvents:UIControlEventTouchUpInside];
    
    
    UILabel*curPagelb = [[UILabel alloc] initWithFrame:CGRectMake(385, 10, 70, 30)];
    curPagelb.backgroundColor = [UIColor clearColor];
    curPagelb.font = [UIFont systemFontOfSize:14];
    curPagelb.textColor = [UIColor blackColor];
    curPagelb.text = @"第          页";
    [botContentView addSubview:curPagelb];
    curPagelb.userInteractionEnabled = YES;
    
    curPageField = [[UITextField alloc] initWithFrame:CGRectMake(15, 0, 40, 30)];
    curPageField.textColor = [UIColor blackColor];
    curPageField.font = [UIFont systemFontOfSize:14];
    [curPagelb addSubview:curPageField];
    curPageField.textAlignment = NSTextAlignmentCenter;
    curPageField.borderStyle = UITextBorderStyleRoundedRect;
    curPageField.delegate = self;
    curPageField.text = @"1";
    
    UIButton*jumpBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [jumpBtn setTitle:@"跳转" forState:UIControlStateNormal];
    [jumpBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [jumpBtn setFrame:CGRectMake(465, 5, 50, 40)];
    [botContentView addSubview:jumpBtn];
    [jumpBtn addTarget:self action:@selector(jumpBtnAction) forControlEvents:UIControlEventTouchUpInside];
    
    
    inputPageView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:inputPageView];
    inputPageView.backgroundColor = [UIColor whiteColor];
    
    inputPageField = [[UITextField alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-80, 40)];
    inputPageField.textColor = [UIColor blackColor];
    inputPageField.font = [UIFont systemFontOfSize:14];
    inputPageField.keyboardType = UIKeyboardTypeNumberPad;
    [inputPageView addSubview:inputPageField];
    
    UIButton*doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [doneBtn setTitle:@"完成" forState:UIControlStateNormal];
    [doneBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [doneBtn setFrame:CGRectMake(self.view.frame.size.width-70, 10, 50, 40)];
    [inputPageView addSubview:doneBtn];
    [doneBtn addTarget:self action:@selector(doneBtnAction) forControlEvents:UIControlEventTouchUpInside];
    
    inputPageView.hidden = YES;
    
    
    helper=[[WebServices alloc] initWithDelegate:self];
    page = 1;
    [self getHisDate];
}
-(void)doneBtnAction{
    inputPageView.hidden = YES;
    curPageField.text = inputPageField.text;
    [inputPageField resignFirstResponder];
}
-(void)jumpBtnAction{
    if (!curPageField.text || [curPageField.text isEqualToString:@""]) {
        [UIAlertView ShowAlertViewWithMsg:@"请输入页数"];
        return;
    }
    if (curPageField.text &&  ![curPageField.text isEqualToString:@""] && [curPageField.text intValue] > totoalPage) {
        [UIAlertView ShowAlertViewWithMsg:[NSString stringWithFormat:@"总页数为:%d,请重新输入",totoalPage]];
        return;
    }
    page = [curPageField.text intValue];
    [self getHisDate];
    
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    inputPageView.hidden = NO;
    inputPageField.text = textField.text;
    [inputPageField becomeFirstResponder];
    return NO;
}
#pragma mark 第一页
-(void)leftBntAction{
    page = 1;
    [self getHisDate];
}
#pragma mark 上一页
-(void)left1BntAction{
    if (page<=1) {
        [UIAlertView ShowAlertViewWithMsg:@"已到达第一页"];
        return;
    }
    page --;
    [self getHisDate];
}
#pragma mark 下一页
-(void)rightBntAction{
    page ++;
    [self getHisDate];
}
#pragma mark 最后一页
-(void)right1BntAction{
    page = totoalPage;
    [self getHisDate];
}

  /*
 HisDate(string startTime, string endTime, string areaID, string stationName, int deviceID, int pageSize, int pageIndex, int type, int stationType, out int count)
 作用	换热站历史数据查询
 参数	startTime	开始时间  yyyy-mm-dd hh:mm:ss
 endTime	结束时间
 pageSize	每页可以显示的行数
 pageIndex	第几页
 stationName	换热站名称
 type	查询类型 0/1    --0:分钟;1:小时
 count	返回查询的记录数
 deviceID	设备ID
 areaID	区域ID
 stationType	站点类型 0/1  普通换热站/智能站
 
 */
- (void)getHisDate{
    
    if (!dataArr) {
        dataArr = [NSMutableArray array];
    }
    [dataArr removeAllObjects];
    [maintable reloadData];
    
    [AppHelper showHUD:@"正在获取换热站数据" inView:self.view];
    NSMutableArray *arr=[NSMutableArray array];
//    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:curArea,@"areaID", nil]];
    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"20",@"pageSize", nil]];
    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%d",page],@"pageIndex", nil]];
    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"0",@"stationType", nil]];
    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:curstartTime,@"startTime", nil]];
    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:curendTime,@"endTime", nil]];
    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:curstationName,@"stationName", nil]];
    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:curhour?@"1":@"0",@"type", nil]];
//    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:curdevid,@"deviceID", nil]];

    NSLog(@"%@",arr);
    
    NSString *soapMsg=[SoapHelper arrayToDefaultSoapMessage:arr methodName:@"HisDate"];
    [helper asyncServiceMethod:@"HisDate" soapMessage:soapMsg];
}
-(void)requestFinishedMessageForCount:(NSString *)count{
    NSInteger totoal = [count intValue]/20;
    if ([count intValue]%20!=0) {
        totoal++;
    }
    totoalPage = totoal;
    totoalPagelb.text = [NSString stringWithFormat:@"共 %d 页",totoal];
}
#pragma mark -
#pragma mark WebServices delegate Methods
-(void)requestFinishedMessage:(NSString*)xml{
    [AppHelper removeHUD];
    NSData*data = [Base64 decodeBase64WithString:xml];
    NSError *error = nil;
    NSData*tempData = [ASIDataDecompressor uncompressData:data error:&error];
    
    NSStringEncoding gbkEncoding = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
    {
        NSString *xmlstr = [[NSString alloc] initWithData:tempData encoding:gbkEncoding];
        NSLog(@"返回的xml数据:%@",xmlstr);
        NSString*preSrt = [xmlstr componentsSeparatedByString:@"@"][0];
        NSLog(@"preSrt:%@",preSrt);
        NSString*sufSrt = [xmlstr componentsSeparatedByString:@"@"][1];
        
        if (!dataArr) {
            dataArr = [NSMutableArray array];
        }
        [dataArr removeAllObjects];
        
        dataArr = [[sufSrt JSONValue] mutableCopy];
        [maintable reloadData];
    }
    
    if (error) {
        NSLog(@"%@",[error description]);
    }
}
-(void)requestFailedMessage:(NSError*)error{
    [AppHelper removeHUD];
    NSLog(@"请求失败:%@\n",[error description]);
}

-(void)requestWithStationName:(NSString *)stationName deviceid:(NSString *)devid curArea:(NSString*)Area startTime:(NSString *)startTime endTime:(NSString *)endTime housSwitch:(BOOL)hour{
    
    curArea = Area;
    curdevid = devid;
    curstartTime = startTime;
    curendTime = endTime;
    curstationName = stationName;
    curhour = hour;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArr.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40)];
    
    UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40)];
    lb.text = @"  时间            名称      属性     一供温       一回温     一供压     一回压        二供温     二回温       二供压       二回压     设备号       阀门开度       液位";
    [headview addSubview:lb];
    
    return headview;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cellid = @"12";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellid];
        float dt = 80;
        {
            UILabel*lb=[[UILabel alloc] initWithFrame:CGRectMake(5, 0, 70, 40)];
            lb.textColor = [UIColor blackColor];
            lb.numberOfLines = 2;
            lb.font = [UIFont systemFontOfSize:14];
            [cell.contentView addSubview:lb];
            lb.tag = 10;
            lb.adjustsFontSizeToFitWidth = YES;
        }
        {
            UILabel*lb=[[UILabel alloc] initWithFrame:CGRectMake(5+80, 0, 70, 40)];
            lb.textColor = [UIColor blackColor];
            lb.numberOfLines = 2;
            lb.font = [UIFont systemFontOfSize:14];
            [cell.contentView addSubview:lb];
            lb.tag = 11;
            lb.adjustsFontSizeToFitWidth = YES;
        }
        {
            UILabel*lb=[[UILabel alloc] initWithFrame:CGRectMake(5+80*2, 0, 70, 40)];
            lb.textColor = [UIColor blackColor];
            lb.numberOfLines = 2;
            lb.font = [UIFont systemFontOfSize:14];
            [cell.contentView addSubview:lb];
            lb.tag = 12;
            lb.adjustsFontSizeToFitWidth = YES;
        }
        {
            UILabel*lb=[[UILabel alloc] initWithFrame:CGRectMake(5+70*2+dt, 0, 70, 40)];
            lb.textColor = [UIColor blackColor];
            lb.numberOfLines = 2;
            lb.font = [UIFont systemFontOfSize:14];
            [cell.contentView addSubview:lb];
            lb.tag = 13;
            lb.adjustsFontSizeToFitWidth = YES;
        }
        {
            UILabel*lb=[[UILabel alloc] initWithFrame:CGRectMake(5+70*2+dt*2, 0, 70, 40)];
            lb.textColor = [UIColor blackColor];
            lb.numberOfLines = 2;
            lb.font = [UIFont systemFontOfSize:14];
            [cell.contentView addSubview:lb];
            lb.tag = 14;
            lb.adjustsFontSizeToFitWidth = YES;
        }
        {
            UILabel*lb=[[UILabel alloc] initWithFrame:CGRectMake(5+70*2+dt*3, 0, 70, 40)];
            lb.textColor = [UIColor blackColor];
            lb.numberOfLines = 2;
            lb.font = [UIFont systemFontOfSize:14];
            [cell.contentView addSubview:lb];
            lb.tag = 15;
            lb.adjustsFontSizeToFitWidth = YES;
        }
        {
            UILabel*lb=[[UILabel alloc] initWithFrame:CGRectMake(5+70*2+dt*4, 0, 70, 40)];
            lb.textColor = [UIColor blackColor];
            lb.numberOfLines = 2;
            lb.font = [UIFont systemFontOfSize:14];
            [cell.contentView addSubview:lb];
            lb.tag = 16;
            lb.adjustsFontSizeToFitWidth = YES;
        }
        {
            UILabel*lb=[[UILabel alloc] initWithFrame:CGRectMake(5+70*2+dt*5, 0, 70, 40)];
            lb.textColor = [UIColor blackColor];
            lb.numberOfLines = 2;
            lb.font = [UIFont systemFontOfSize:14];
            [cell.contentView addSubview:lb];
            lb.tag = 17;
            lb.adjustsFontSizeToFitWidth = YES;
        }
        {
            UILabel*lb=[[UILabel alloc] initWithFrame:CGRectMake(5+70*2+dt*6, 0, 70, 40)];
            lb.textColor = [UIColor blackColor];
            lb.numberOfLines = 2;
            lb.font = [UIFont systemFontOfSize:14];
            [cell.contentView addSubview:lb];
            lb.tag = 18;
            lb.adjustsFontSizeToFitWidth = YES;
            
        }
        {
            UILabel*lb=[[UILabel alloc] initWithFrame:CGRectMake(5+70*2+dt*7, 0, 70, 40)];
            lb.textColor = [UIColor blackColor];
            lb.numberOfLines = 2;
            lb.font = [UIFont systemFontOfSize:14];
            [cell.contentView addSubview:lb];
            lb.tag = 19;
            lb.adjustsFontSizeToFitWidth = YES;
        }
        {
            UILabel*lb=[[UILabel alloc] initWithFrame:CGRectMake(5+70*2+dt*8, 0, 70, 40)];
            lb.textColor = [UIColor blackColor];
            lb.numberOfLines = 2;
            lb.font = [UIFont systemFontOfSize:14];
            [cell.contentView addSubview:lb];
            lb.tag = 20;
            lb.adjustsFontSizeToFitWidth = YES;
        }
        {
            UILabel*lb=[[UILabel alloc] initWithFrame:CGRectMake(5+70*2+dt*9+15, 0, 70, 40)];
            lb.textColor = [UIColor blackColor];
            lb.numberOfLines = 2;
            lb.font = [UIFont systemFontOfSize:14];
            [cell.contentView addSubview:lb];
            lb.tag = 21;
            lb.adjustsFontSizeToFitWidth = YES;
        }
        {
            UILabel*lb=[[UILabel alloc] initWithFrame:CGRectMake(5+70*2+dt*10, 0, 70, 40)];
            lb.textColor = [UIColor blackColor];
            lb.numberOfLines = 2;
            lb.font = [UIFont systemFontOfSize:14];
            [cell.contentView addSubview:lb];
            lb.tag = 22;
            lb.adjustsFontSizeToFitWidth = YES;
        }
        {
            UILabel*lb=[[UILabel alloc] initWithFrame:CGRectMake(5+70*2+dt*11+25, 0, 70, 40)];
            lb.textColor = [UIColor blackColor];
            lb.numberOfLines = 2;
            lb.font = [UIFont systemFontOfSize:14];
            [cell.contentView addSubview:lb];
            lb.tag = 23;
            lb.adjustsFontSizeToFitWidth = YES;
        }
        {
            UILabel*lb=[[UILabel alloc] initWithFrame:CGRectMake(5+70*2+dt*12+20, 0, 70, 40)];
            lb.textColor = [UIColor blackColor];
            lb.numberOfLines = 2;
            lb.font = [UIFont systemFontOfSize:14];
            [cell.contentView addSubview:lb];
            lb.tag = 24;
            lb.adjustsFontSizeToFitWidth = YES;
        }
     }
    {
        UILabel*lb=(UILabel *)[cell.contentView viewWithTag:10];
        lb.text = [dataArr[indexPath.row] objectAtIndex:12];
    }
    {
        UILabel*lb=(UILabel *)[cell.contentView viewWithTag:11];
        lb.text = [dataArr[indexPath.row] objectAtIndex:0];
    }
    {
        UILabel*lb=(UILabel *)[cell.contentView viewWithTag:12];
        lb.text = [dataArr[indexPath.row] objectAtIndex:1];
    }
    {
        UILabel*lb=(UILabel *)[cell.contentView viewWithTag:13];
        lb.text = [NSString stringWithFormat:@"%f", [[dataArr[indexPath.row] objectAtIndex:2] floatValue]];
    }
    {
        UILabel*lb=(UILabel *)[cell.contentView viewWithTag:14];
        lb.text = [NSString stringWithFormat:@"%f", [[dataArr[indexPath.row] objectAtIndex:3] floatValue]];
    }
    {
        UILabel*lb=(UILabel *)[cell.contentView viewWithTag:15];
        lb.text = [NSString stringWithFormat:@"%f", [[dataArr[indexPath.row] objectAtIndex:4] floatValue]];
    }
    {
        UILabel*lb=(UILabel *)[cell.contentView viewWithTag:16];
        lb.text = [NSString stringWithFormat:@"%f", [[dataArr[indexPath.row] objectAtIndex:5] floatValue]];
    }
    {
        UILabel*lb=(UILabel *)[cell.contentView viewWithTag:17];
        lb.text = [NSString stringWithFormat:@"%f", [[dataArr[indexPath.row] objectAtIndex:6] floatValue]];
    }
    {
        UILabel*lb=(UILabel *)[cell.contentView viewWithTag:18];
        lb.text = [NSString stringWithFormat:@"%f", [[dataArr[indexPath.row] objectAtIndex:7] floatValue]];
    }
    {
        UILabel*lb=(UILabel *)[cell.contentView viewWithTag:19];
        lb.text = [NSString stringWithFormat:@"%f", [[dataArr[indexPath.row] objectAtIndex:8] floatValue]];
    }
    {
        UILabel*lb=(UILabel *)[cell.contentView viewWithTag:20];
        lb.text = [NSString stringWithFormat:@"%f", [[dataArr[indexPath.row] objectAtIndex:9] floatValue]];
    }
    {
        UILabel*lb=(UILabel *)[cell.contentView viewWithTag:21];
        lb.text = [NSString stringWithFormat:@"%d", [[dataArr[indexPath.row] objectAtIndex:13] intValue]];
    }
    {
        UILabel*lb=(UILabel *)[cell.contentView viewWithTag:22];
        lb.text = [NSString stringWithFormat:@"%f", [[dataArr[indexPath.row] objectAtIndex:10] floatValue]];
    }
    {
        UILabel*lb=(UILabel *)[cell.contentView viewWithTag:23];
        NSNumber *value = [dataArr[indexPath.row] objectAtIndex:11];
        if (![value isEqual:[NSNull null]]) {
            lb.text = [NSString stringWithFormat:@"%.02f", [value floatValue]];
        }
    }
//        {
//        UILabel*lb=(UILabel *)[cell.contentView viewWithTag:24];
//            lb.text = [NSString stringWithFormat:@"%.02f", [[dataArr[indexPath.row] objectAtIndex:11] floatValue]];
//        }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
//    NormalStationDetailViewController*ctrl = [[NormalStationDetailViewController alloc] initWithNibName:@"NormalStationDetailViewController" bundle:nil];
//    ctrl.stationName = dataArr[indexPath.row][1];
//    [self presentViewController:ctrl animated:NO completion:nil];
}
-(BOOL)shouldAutorotate{
    return YES;
}
-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscapeRight;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
