//
//  NormalStationDetailViewController.m
//  DongYuan
//
//  Created by 鄢罗军 on 14/10/11.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import "NormalStationDetailViewController.h"
#import "SoapHelper.h"
#import "ASIDataDecompressor.h"
#import "Base64.h"
#import "JSON.h"

@interface NormalStationDetailViewController ()

@end

@implementation NormalStationDetailViewController
@synthesize stationName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)backAction{
    [self dismissViewControllerAnimated:NO completion:nil];
}
- (void)getDeviceByStationName{
    
    [AppHelper showHUD:@"正在获取换热站数据" inView:self.view];
    NSMutableArray *arr=[NSMutableArray array];
    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:stationName?stationName:@"",@"stationName", nil]];
     NSString *soapMsg=[SoapHelper arrayToDefaultSoapMessage:arr methodName:@"DeviceByStationName"];
    [helper asyncServiceMethod:@"DeviceByStationName" soapMessage:soapMsg];
}
- (void)getNowDataAllByDeviceID:(NSString *)devid{
    
    if (!dataArr) {
        dataArr = [NSMutableArray array];
    }
    [dataArr removeAllObjects];
    [maintable reloadData];
    
    [AppHelper showHUD:@"正在获取换热站数据" inView:self.view];
    NSMutableArray *arr=[NSMutableArray array];
    [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:devid?devid:@"",@"deviceID", nil]];
    NSString *soapMsg=[SoapHelper arrayToDefaultSoapMessage:arr methodName:@"NowDataAllByDeviceID"];
    [helper asyncServiceMethod:@"NowDataAllByDeviceID" soapMessage:soapMsg];
    
    isGetData = YES;
}

#pragma mark -
#pragma mark WebServices delegate Methods
-(void)requestFinishedMessage:(NSString*)xml{
    [AppHelper removeHUD];
    NSData*data = [Base64 decodeBase64WithString:xml];
    NSError *error = nil;
    NSData*tempData = [ASIDataDecompressor uncompressData:data error:&error];
    
    NSStringEncoding gbkEncoding = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
    {
        NSString *xmlstr = [[NSString alloc] initWithData:tempData encoding:gbkEncoding];
        NSLog(@"返回的xml数据:%@",xmlstr);
        NSString*preSrt = [xmlstr componentsSeparatedByString:@"@"][0];
        NSLog(@"preSrt:%@",preSrt);
        NSString*sufSrt = [xmlstr componentsSeparatedByString:@"@"][1];
        
        if (!dataArr) {
            dataArr = [NSMutableArray array];
        }
        [dataArr removeAllObjects];
        
        dataArr = [[sufSrt JSONValue] mutableCopy];
        
        if (isGetData) {
            isGetData = NO;
            dataArr = [[sufSrt JSONValue] mutableCopy][0];
            [maintable reloadData];
            return;
        }
        
        NSString *devid = @"";
        for (NSArray *ar in dataArr) {
            if ([[ar lastObject] isEqualToString:@"普通"]) {
                titlelb.text = [NSString stringWithFormat:@"设备 %d",[ar[0] intValue]];
                devid = [NSString stringWithFormat:@"%d",[ar[0] intValue]];
                break;
            }
        }
      
        [self getNowDataAllByDeviceID:devid];
    }
    
    if (error) {
        NSLog(@"%@",[error description]);
    }
}
-(void)requestFailedMessage:(NSError*)error{
    [AppHelper removeHUD];
    NSLog(@"请求失败:%@\n",[error description]);
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self AddCustomNavigationBar];
    [self AddCustomLeftImageItem:[UIImage imageNamed:@"返回"] WithSelector:@selector(backAction) Target:self];
   titlelb = [self AddCustomNavigationbarTitle:@"天气情况"];
    
    {
    UILabel*line = [[UILabel alloc] initWithFrame:CGRectMake(0, GetToTopDistance+9.5, ScreenFrame.size.width, 0.5)];
    line.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:line];
    }
    {
        UILabel*line = [[UILabel alloc] initWithFrame:CGRectMake(0, GetToTopDistance+35+10.5, ScreenFrame.size.width, 0.5)];
        line.backgroundColor = [UIColor lightGrayColor];
        [self.view addSubview:line];
    }
    {
        UILabel*line = [[UILabel alloc] initWithFrame:CGRectMake(ScreenFrame.size.width/4, GetToTopDistance+10, 0.5, 35)];
        line.backgroundColor = [UIColor lightGrayColor];
        [self.view addSubview:line];
    }
    {
        UILabel*line = [[UILabel alloc] initWithFrame:CGRectMake((ScreenFrame.size.width/4)*2+0.5, GetToTopDistance+10, 0.5, 35)];
        line.backgroundColor = [UIColor lightGrayColor];
        [self.view addSubview:line];
    }

    {
        UILabel*line = [[UILabel alloc] initWithFrame:CGRectMake((ScreenFrame.size.width/4)*3+1, GetToTopDistance+10, 0.5, 35)];
        line.backgroundColor = [UIColor lightGrayColor];
        [self.view addSubview:line];
    }

    
    UIView*bgview = [[UIView alloc] initWithFrame:CGRectMake(0, GetToTopDistance+10, ScreenFrame.size.width, 35)];
    [self.view addSubview:bgview];
    
    btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn1 setTitle:@"普通数据" forState:UIControlStateNormal];
    [btn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn1 setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [btn1 addTarget:self action:@selector(btn1Action) forControlEvents:UIControlEventTouchUpInside];
    btn1.frame = CGRectMake(0, 0, ScreenFrame.size.width/4, 35);
    [btn1 setBackgroundColor:[UIColor ColorWithHexString:@"0084ff"]];
    [bgview addSubview:btn1];
    [btn1 setSelected:YES];
    
    btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn2 setTitle:@"水耗" forState:UIControlStateNormal];
    [btn2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn2 setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [btn2 addTarget:self action:@selector(btn2Action) forControlEvents:UIControlEventTouchUpInside];
    [bgview addSubview:btn2];
    btn2.frame = CGRectMake(ScreenFrame.size.width/4+0.5, 0, ScreenFrame.size.width/4-0.5, 35);

    btn3 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn3 setTitle:@"电耗" forState:UIControlStateNormal];
    [btn3 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn3 setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [btn3 addTarget:self action:@selector(btn3Action) forControlEvents:UIControlEventTouchUpInside];
    [bgview addSubview:btn3];
    btn3.frame = CGRectMake((ScreenFrame.size.width/4)*2+1, 0, ScreenFrame.size.width/4-0.5, 35);

    btn4 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn4 setTitle:@"水电热" forState:UIControlStateNormal];
    [btn4 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn4 setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [btn4 addTarget:self action:@selector(btn4Action) forControlEvents:UIControlEventTouchUpInside];
    [bgview addSubview:btn4];
    btn4.frame = CGRectMake((ScreenFrame.size.width/4)*3+1.5, 0, ScreenFrame.size.width/4-0.5, 35);

    helper=[[WebServices alloc] initWithDelegate:self];
    [self getDeviceByStationName];
    
    [self ReCreatTableView];
    
    curIndex = 0;
}
-(void)ReCreatTableView{
    if (maintable) {
        [maintable removeFromSuperview];
        maintable = nil;
    }
    maintable = [[UITableView alloc] initWithFrame:CGRectMake(0, GetToTopDistance+10+35+1, ScreenFrame.size.width, ScreenFrame.size.height-(GetToTopDistance+10+35+1)) style:UITableViewStylePlain];
    maintable.dataSource = self;
    maintable.delegate = self;
    [self.view addSubview:maintable];
}
-(void)btn1Action{
    [btn1 setSelected:YES];
    [btn2 setSelected:NO];
    [btn3 setSelected:NO];
    [btn4 setSelected:NO];
    
    [btn1 setBackgroundColor:[UIColor ColorWithHexString:@"0084ff"]];
    [btn2 setBackgroundColor:[UIColor whiteColor]];
    [btn3 setBackgroundColor:[UIColor whiteColor]];
    [btn4 setBackgroundColor:[UIColor whiteColor]];

    curIndex = 0;
    [self ReCreatTableView];
}
-(void)btn2Action{
    [btn1 setSelected:NO];
    [btn2 setSelected:YES];
    [btn3 setSelected:NO];
    [btn4 setSelected:NO];
    
    [btn1 setBackgroundColor:[UIColor whiteColor]];
    [btn2 setBackgroundColor:[UIColor ColorWithHexString:@"0084ff"]];
    [btn3 setBackgroundColor:[UIColor whiteColor]];
    [btn4 setBackgroundColor:[UIColor whiteColor]];
    
    curIndex = 1;
    [self ReCreatTableView];
}
-(void)btn3Action{
    [btn1 setSelected:NO];
    [btn2 setSelected:NO];
    [btn3 setSelected:YES];
    [btn4 setSelected:NO];
    
    [btn3 setBackgroundColor:[UIColor ColorWithHexString:@"0084ff"]];
    [btn2 setBackgroundColor:[UIColor whiteColor]];
    [btn1 setBackgroundColor:[UIColor whiteColor]];
    [btn4 setBackgroundColor:[UIColor whiteColor]];
    
    curIndex = 2;
    [self ReCreatTableView];
}
-(void)btn4Action{
    [btn1 setSelected:NO];
    [btn2 setSelected:NO];
    [btn3 setSelected:NO];
    [btn4 setSelected:YES];
    
    [btn4 setBackgroundColor:[UIColor ColorWithHexString:@"0084ff"]];
    [btn2 setBackgroundColor:[UIColor whiteColor]];
    [btn3 setBackgroundColor:[UIColor whiteColor]];
    [btn1 setBackgroundColor:[UIColor whiteColor]];
    
    curIndex = 3;
    [self ReCreatTableView];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (curIndex == 0) {
        return 13;
    }
    if (curIndex ==1) {
        return 1;
    }
    if (curIndex ==2) {
        return 1;
    }
    if (curIndex ==3) {
        return 4;
    }
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cellid = [NSString stringWithFormat:@"cellid%d",indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellid];
        {
            UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, 100, 34)];
            lb.backgroundColor = [UIColor clearColor];
            lb.font = [UIFont systemFontOfSize:14];
            [cell.contentView addSubview:lb];
            lb.textAlignment = NSTextAlignmentCenter;
            lb.tag = 10;
        }
        {
            UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(160, 5, 160, 34)];
            lb.backgroundColor = [UIColor clearColor];
            lb.font = [UIFont systemFontOfSize:14];
            [cell.contentView addSubview:lb];
            lb.tag = 11;
            lb.textAlignment = NSTextAlignmentCenter;
            lb.adjustsFontSizeToFitWidth = YES;
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    UILabel *lb1 = (UILabel *)[cell.contentView viewWithTag:10];
    UILabel *lb2 = (UILabel *)[cell.contentView viewWithTag:11];
    if (dataArr.count == 0) {
        return cell;
    }
    if (curIndex == 0) {
        switch (indexPath.row) {
            case 0:
            {
                lb1.text = @"换热站名称";
                lb2.text = dataArr[1];
            }
                break;
            case 1:
            {
                lb1.text = @"供热面积";
                if ([dataArr[12] isEqual:[NSNull null]]) {
                    lb2.text = @"";
                }else{
                    lb2.text = [NSString stringWithFormat:@"%f",[dataArr[12] floatValue]];
                }
            }
                break;
            case 2:
            {
                lb1.text = @"时间";
                if ([dataArr[0] isEqual:[NSNull null]]) {
                    lb2.text = @"";
                }else{
                    lb2.text = dataArr[0];
                }
             }
                break;
            case 3:
            {
                lb1.text = @"设备号";
                if ([[dataArr lastObject] isEqual:[NSNull null]]) {
                    lb2.text = @"";
                }else{
                    lb2.text = [NSString stringWithFormat:@"%d",[[dataArr lastObject] intValue]];
                }
             }
                break;
            case 4:
            {
                lb1.text = @"设备属性";
                if ([dataArr[2] isEqual:[NSNull null]]) {
                    lb2.text = @"";
                }else{
                     lb2.text = dataArr[2];
                 }

            }
                break;
            case 5:
            {
                lb1.text = @"一供温";
                if ([dataArr[3] isEqual:[NSNull null]]) {
                    lb2.text = @"";
                }else{
                    lb2.text = [NSString stringWithFormat:@"%f",[dataArr[3] floatValue]];
                }

            }
                break;
            case 6:
            {
                
                lb1.text = @"一回温";
                if ([dataArr[4] isEqual:[NSNull null]]) {
                    lb2.text = @"";
                }else{
                    lb2.text = [NSString stringWithFormat:@"%f",[dataArr[4] floatValue]];
                }

            }
                break;
            case 7:
            {
                lb1.text = @"一供压";
                if ([dataArr[5] isEqual:[NSNull null]]) {
                    lb2.text = @"";
                }else{
                    lb2.text = [NSString stringWithFormat:@"%f",[dataArr[5] floatValue]];
                }
                
            }
                break;
            case 8:
            {
                
                lb1.text = @"一回压";
                if ([dataArr[6] isEqual:[NSNull null]]) {
                    lb2.text = @"";
                }else{
                    lb2.text = [NSString stringWithFormat:@"%f",[dataArr[6] floatValue]];
                }
                
            }
                break;
            case 9:
            {
                lb1.text = @"二供温";
                if ([dataArr[7] isEqual:[NSNull null]]) {
                    lb2.text = @"";
                }else{
                    lb2.text = [NSString stringWithFormat:@"%f",[dataArr[7] floatValue]];
                }
                
            }
                break;
            case 10:
            {
                
                lb1.text = @"二回温";
                if ([dataArr[8] isEqual:[NSNull null]]) {
                    lb2.text = @"";
                }else{
                    lb2.text = [NSString stringWithFormat:@"%f",[dataArr[8] floatValue]];
                }
                
            }
                break;
            case 11:
            {
                lb1.text = @"二供压";
                if ([dataArr[9] isEqual:[NSNull null]]) {
                    lb2.text = @"";
                }else{
                    lb2.text = [NSString stringWithFormat:@"%f",[dataArr[9] floatValue]];
                }
                
            }
                break;
            case 12:
            {
                
                lb1.text = @"二回压";
                if ([dataArr[10] isEqual:[NSNull null]]) {
                    lb2.text = @"";
                }else{
                    lb2.text = [NSString stringWithFormat:@"%f",[dataArr[10] floatValue]];
                }
                
            }
                break;

            default:
                break;
        }
    }
    if (curIndex == 1) {
        lb1.text = @"水耗";
        if ([dataArr[18] isEqual:[NSNull null]]) {
            lb2.text = @"";
        }else{
            lb2.text = [NSString stringWithFormat:@"%f", [dataArr[18] floatValue]];
        }
     }
    if (curIndex == 2) {
        lb1.text = @"电耗";
        if ([dataArr[19] isEqual:[NSNull null]]) {
            lb2.text = @"";
        }else{
            lb2.text = [NSString stringWithFormat:@"%f", [dataArr[19] floatValue]];
        }
    }
    if (curIndex == 3) {
        switch (indexPath.row) {
            case 0:
            {
                lb1.text = @"流量";
                 if ([dataArr[14] isEqual:[NSNull null]]) {
                    lb2.text = @"";
                }else{
                    lb2.text = [NSString stringWithFormat:@"%f",[dataArr[14] floatValue]];
                }

            }
                break;
            case 1:
            {
                lb1.text = @"站总流量";
                if ([dataArr[15] isEqual:[NSNull null]]) {
                    lb2.text = @"";
                }else{
                    lb2.text = [NSString stringWithFormat:@"%f",[dataArr[15] floatValue]];
                }
            }
                break;
            case 2:
            {
                lb1.text = @"热量";
                if ([dataArr[16] isEqual:[NSNull null]]) {
                    lb2.text = @"";
                }else{
                    lb2.text = [NSString stringWithFormat:@"%f",[dataArr[16] floatValue]];
                }

            }
                break;
            case 3:
            {
                lb1.text = @"累计热量";
                if ([dataArr[17] isEqual:[NSNull null]]) {
                    lb2.text = @"";
                }else{
                    lb2.text = [NSString stringWithFormat:@"%f",[dataArr[17] floatValue]];
                }

            }
                break;
         }
    }
    
    return cell;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL)shouldAutorotate{
    return YES;
}
-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;//这里写你需要的方向。看好了，别写错了
}
@end



