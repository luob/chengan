//
//  NormalStationDetailViewController.h
//  DongYuan
//
//  Created by 鄢罗军 on 14/10/11.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import "BASEUIViewController.h"

#import "WebServices.h"
@interface NormalStationDetailViewController : BASEUIViewController<WebServiceDelegate,UITableViewDataSource,UITableViewDelegate>{
    UITableView *maintable;
    NSMutableArray *dataArr;
    UILabel *titlelb;
    
    UIButton*btn1,*btn2,*btn3,*btn4;
    
    WebServices *helper;
    BOOL isGetData;
    
    NSInteger curIndex;
}
@property(nonatomic,retain) NSString*stationName;

@end
