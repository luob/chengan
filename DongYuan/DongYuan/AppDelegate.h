//
//  AppDelegate.h
//  DongYuan
//
//  Created by 鄢罗军 on 14/9/19.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginViewController.h"
#import "constants.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,assign) CGFloat ToTopDistance,TopHeight;
@property (nonatomic, assign)BOOL iphone4,iphone5;

@property (nonatomic,retain) NSMutableArray*areaArrs;
@property (nonatomic,retain) NSString *WebServiceUrl;

@end
