//
//  constants.h
//  DongYuan
//
//  Created by 鄢罗军 on 14/9/19.
//  Copyright (c) 2014年 鄢罗军. All rights reserved.
//

#ifndef DongYuan_constants_h
#define DongYuan_constants_h

#import "Appdelegate.h"

#define SharedDelegate  ((AppDelegate *)[[UIApplication sharedApplication] delegate])
#define GetTopHeight (SharedDelegate.TopHeight)
#define GetToTopDistance (SharedDelegate.ToTopDistance)

#define kTitleFont 18.f
#define ScreenFrame [[UIScreen mainScreen] bounds]
#define IOS7 [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0?YES:NO

#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)
#define iPhone4 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)


#define kWeatherID @"101030100"
#endif
